=begin
=MessageClearRecentsアクション

新着メッセージリストをクリアします。新着メッセージリストについては、((<新着通知|URL:Recents.html>))を参照してください。


==引数
なし


==有効なウィンドウ・ビュー
*メインウィンドウ

=end
