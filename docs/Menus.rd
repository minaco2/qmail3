=begin
=メニュー

各ウィンドウにはメニューバーがあり、各ビューには右クリックすると表示されるコンテキストメニューが用意されています。


==メニューバー

*((<メインウィンドウのメニューバー|URL:MainWindowMenuBar.html>))
*((<メッセージウィンドウのメニューバー|URL:MessageWindowMenuBar.html>))
*((<エディットウィンドウのメニューバー|URL:EditWindowMenuBar.html>))
*((<アドレス帳ウィンドウのメニューバー|URL:AddressBookWindowMenuBar.html>))


==コンテキストメニュー

*((<フォルダビューのコンテキストメニュー|URL:FolderMenu.html>))
*((<フォルダコンボボックスのコンテキストメニュー|URL:FolderMenu.html>))
*((<フォルダリストビューのコンテキストメニュー|URL:FolderListMenu.html>))
*((<リストビューのコンテキストメニュー|URL:ListMenu.html>))
*((<タブのコンテキストメニュー|URL:TabMenu.html>))
*((<プレビューのコンテキストメニュー|URL:MessageMenu.html>))
*((<メッセージビューのコンテキストメニュー|URL:MessageMenu.html>))
*((<添付ファイルのコンテキストメニュー|URL:AttachmentMenu.html>))
*((<添付ファイル編集のコンテキストメニュー|URL:AttachmentEditMenu.html>))
*((<エディットビューのコンテキストメニュー|URL:EditMenu.html>))
*((<アドレスビューのコンテキストメニュー|URL:AddressMenu.html>))

=end
