=begin
=[フォント]ダイアログ

フォントを指定するダイアログです。

((<[フォント]ダイアログ|"IMG:images/FontDialog.png">))


+[フォント]
フォントの名前を指定します。


+[スタイル]
フォントのスタイルを指定します。


+[サイズ]
フォントのサイズをポイント単位で指定します。

=end
