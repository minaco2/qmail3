=begin
=@False

 Boolean @False()


==説明
Falseを返します。


==引数
なし


==エラー
*引数の数が合っていない場合


==条件
なし


==例
 # False
 # -> False
 @False()

=end
