=begin
=@Add

 Number @Add(Number arg1, Number arg2)


==説明
arg1とarg2の和を返します。


==引数
:Number arg1
  数値

:Number arg2
  数値


==エラー
*引数の数が合っていない場合


==条件
なし


==例
 # 1 + 2
 # -> 3
 @Add(1, 2)

=end
