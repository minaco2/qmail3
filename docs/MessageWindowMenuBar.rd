=begin
=メッセージウィンドウのメニューバー

((<メッセージウィンドウのメニューバー|"IMG:images/MessageWindowMenuBar.png">))


====[ファイル]

+((<[印刷]|URL:FilePrintAction.html>))
メッセージを印刷します。


+((<[閉じる]|URL:FileCloseAction.html>))
メッセージウィンドウを閉じます。


====[編集]

+((<[コピー]|URL:EditCopyAction.html>))
メッセージをクリップボードにコピーします。


+((<[全て選択]|URL:EditSelectAllAction.html>))
テキストを全て選択します。


+((<[削除]|URL:EditDeleteAction.html>))
メッセージを削除します。


+((<[スパムとして削除]|URL:EditDeleteJunkAction.html>))
メッセージをスパムとして削除します。


+((<[メッセージ内検索]|URL:EditFindAction.html>))
メッセージ内のテキストを検索します。


+((<[次を検索]|URL:EditFindNextAction.html>))
直前の検索条件で次を検索します。


+((<[前を検索]|URL:EditFindPrevAction.html>))
直前の検索条件で前を検索します。


====[表示]

+((<[次のメッセージ]|URL:ViewNextMessageAction.html>))
ひとつ後のメッセージを表示します。


+((<[前のメッセージ]|URL:ViewPrevMessageAction.html>))
ひとつ前のメッセージを表示します。


+((<[次の未読メッセージ]|URL:ViewNextUnseenMessageAction.html>))
次の未読メッセージを表示します。


+[モード]

*((<[すべて表示]|URL:ViewRawModeAction.html>))
 
 メッセージ全体をテキストで表示します。

*((<[ソース表示]|URL:ViewSourceModeAction.html>))
 
 メッセージのソースを表示します。

*((<[引用を線で表示]|URL:ViewQuoteModeAction.html>))
 
 引用を線で表示します。

*((<[キャレットを表示]|URL:ViewSelectModeAction.html>))
 
 キャレットを表示します。

*((<"[S/MIME]"|URL:ViewSMIMEModeAction.html>))
 
 ((<"S/MIME"|URL:SMIME.html>))の復号と署名の検証を行います。

*((<[PGP]|URL:ViewPGPModeAction.html>))
 
 ((<PGP|URL:PGP.html>))の復号と署名の検証を行います。


+[テンプレート]

*((<[なし]|URL:ViewTemplateAction.html>))
 
 ((<表示用のテンプレート|URL:ViewTemplate.html>))を使わずに表示します。

*[<テンプレート名>]
 
 <テンプレート名>の((<表示用のテンプレート|URL:ViewTemplate.html>))を使って表示します。


+[エンコーディング]

*((<[自動判定]|URL:ViewEncodingAction.html>))
 
 自動判定されたエンコーディングで表示します。

*((<[<エンコーディング名>]|URL:ViewEncodingAction.html>))
 
 <エンコーディング名>のエンコーディングで表示します。


+[HTML]

*((<[HTMLを表示]|URL:ViewHtmlModeAction.html>))
 
 HTMLをブラウザコントロールで表示します。

*((<[オンラインで表示]|URL:ViewHtmlOnlineModeAction.html>))
 
 HTML中に埋め込まれた画像などをインターネットから取得します。

*((<[インターネットゾーンで表示]|URL:ViewHtmlInternetZoneModeAction.html>))
 
 インターネットゾーンで表示します。

*((<[最大サイズ]|URL:ViewZoomAction.html>))
 
 文字サイズを最大にします。

*((<[大サイズ]|URL:ViewZoomAction.html>))
 
 文字サイズを大にします。

*((<[中サイズ]|URL:ViewZoomAction.html>))
 
 文字サイズを中にします。

*((<[小サイズ]|URL:ViewZoomAction.html>))
 
 文字サイズを小にします。

*((<[最小サイズ]|URL:ViewZoomAction.html>))
 
 文字サイズを最小にします。

*((<[デフォルト]|URL:ViewZoomAction.html>))
 
 文字サイズをデフォルトにします。

*((<[大きくする]|URL:ViewZoomAction.html>))
 
 文字サイズを大きくします。

*((<[小さくする]|URL:ViewZoomAction.html>))
 
 文字サイズを小さくします。


+[コントロールの表示]

*((<[ツールバーを表示]|URL:ViewShowToolbarAction.html>))
 
 ツールバーの表示と非表示を切り替えます。

*((<[ステータスバーを表示]|URL:ViewShowStatusBarAction.html>))
 
 ステータスバーの表示と非表示を切り替えます。

*((<[ヘッダを表示]|URL:ViewShowHeaderAction.html>))
 
 ヘッダビューの表示と非表示を切り返します。


====[メッセージ]

+((<[新規]|URL:MessageCreateAction.html>))
新規メッセージを作成します。


+((<[返信]|URL:MessageCreateAction.html>))
メッセージに返信します。


+((<[全員に返信]|URL:MessageCreateAction.html>))
メッセージを全員に返信します。


+((<[転送]|URL:MessageCreateAction.html>))
メッセージを転送します。


+((<[編集]|URL:MessageCreateAction.html>))
メッセージを編集します。


+[テンプレート]

*((<[<テンプレート名>]|URL:MessageCreateAction.html>))
 
 ((<作成用のテンプレート|URL:CreateTemplate.html>))を使用してメッセージを作成します。


+[マーク]

*((<[既読にする]|URL:MessageMarkSeenAction.html>))
 
 メッセージを既読にします。

*((<[未読にする]|URL:MessageUnmarkSeenAction.html>))
 
 メッセージを未読にします。

*((<[マークをつける]|URL:MessageMarkAction.html>))
 
 メッセージにマークをつけます。

*((<[マークを消す]|URL:MessageUnmarkAction.html>))
 
 メッセージのマークを消します。

*((<[ダウンロード予約]|URL:MessageMarkDownloadAction.html>))
 
 メッセージをダウンロード予約します。

*((<[本文をダウンロード予約]|URL:MessageMarkDownloadTextAction.html>))
 
 メッセージのテキスト部分をダウンロード予約します。

*((<[削除マークをつける]|URL:MessageMarkDeletedAction.html>))
 
 メッセージに削除マークをつけます。


+((<[ラベル]|URL:MessageLabelAction.html>))
メッセージのラベルを編集します。


+[移動]

*((<[<フォルダ名>]|URL:MessageMoveAction.html>))
 
 メッセージを<フォルダ名>のフォルダに移動します。

*((<[その他]|URL:MessageMoveAction.html>))
 
 メッセージをダイアログで指定したフォルダに移動します。


+[添付ファイル]

*((<[保存]|URL:MessageDetachAction.html>))
 
 添付ファイルを保存します。

*((<[削除]|URL:MessageDeleteAttachmentAction.html>))
 
 添付ファイルを削除します。

*((<[<添付ファイル名>]|URL:MessageOpenAttachmentAction.html>))
 
 <添付ファイル名>の添付ファイルを関連付けで開きます。

*((<[ダイジェストを展開]|URL:MessageExpandDigestAction.html>))
 
 メッセージがダイジェスト形式の場合に展開します。


+((<[プロパティ]|URL:MessagePropertyAction.html>))
メッセージのプロパティを表示します。


====[ツール]

+[スクリプト]

*((<[<スクリプト名>]|URL:ToolScriptAction.html>))
 
 <スクリプト名>のスクリプトを実行します。

=end
