=begin
=ToolAttachmentアクション

((<添付ファイル|URL:Attachment.html>))を追加・削除するために、((<[添付ファイル]ダイアログ|URL:AttachmentDialog.html>))を開きます。


==引数
なし


==有効なウィンドウ・ビュー
*エディットウィンドウ

=end
