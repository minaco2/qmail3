=begin
=ViewPGPModeアクション

((<セキュリティモード|URL:SecurityMode.html>))のPGPモードをOn/Offします。


==引数
なし


==有効なウィンドウ・ビュー
*メインウィンドウ
*メッセージウィンドウ

=end
