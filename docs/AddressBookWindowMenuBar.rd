=begin
=アドレス帳ウィンドウのメニューバー

((<アドレス帳ウィンドウのメニューバー|"IMG:images/AddressBookWindowMenuBar.png">))

====[ファイル]

+((<[新規]|URL:AddressNewAction.html>))
新しいエントリを追加します。


+((<[編集]|URL:AddressEditAction.html>))
フォーカスのあるエントリを編集します。


+((<[削除]|URL:AddressDeleteAction.html>))
選択されたエントリを削除します。


+((<[保存]|URL:FileSaveAction.html>))
編集した内容を保存します。


+((<[閉じる]|URL:FileCloseAction.html>))
ウィンドウを閉じます。


====[表示]

+[ソート]

*((<[名前]|URL:ViewSortNameAction.html>))
 
 名前でソートします。

*((<[アドレス]|URL:ViewSortAddressAction.html>))
 
 アドレスでソートします。

*((<[コメント]|URL:ViewSortCommentAction.html>))
 
 コメントでソートします。

*((<[昇順]|URL:ViewSortAscendingAction.html>))
 
 昇順でソートします。

*((<[降順]|URL:ViewSortDescendingAction.html>))
 
 降順でソートします。


+((<[再読み込み]|URL:ViewRefreshAction.html>))
アドレス帳をファイルから読み直します。


+[コントロールの表示]

*((<[ツールバーを表示]|URL:ViewShowToolbarAction.html>))
 
 ツールバーの表示と非表示を切り替えます。

*((<[ステータスバーを表示]|URL:ViewShowStatusBarAction.html>))
 
 ステータスバーの表示と非表示を切り替えます。

=end
