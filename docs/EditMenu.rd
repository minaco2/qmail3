=begin
=エディットビューのコンテキストメニュー

((<エディットビューのコンテキストメニュー|"IMG:images/EditMenu.png">))


+((<[元に戻す]|URL:EditUndoAction.html>))
直前の操作を元に戻します。


+((<[やり直す]|URL:EditRedoAction.html>))
元に戻した操作をやり直します。


+((<[切り取り]|URL:EditCutAction.html>))
選択されたテキストを切り取ってクリップボードに入れます。


+((<[コピー]|URL:EditCopyAction.html>))
選択されたテキストをコピーしてクリップボードに入れます。


+((<[貼り付け]|URL:EditPasteAction.html>))
クリップボードからテキストを貼り付けます。


+((<[引用符付き貼り付け]|URL:EditPasteWithQuoteAction.html>))
クリップボードから引用符付きでテキストを貼り付けます。


+((<[すべて選択]|URL:EditSelectAllAction.html>))
全てのテキストを選択します。


+((<[検索]|URL:EditFindAction.html>))
テキストを検索します。


+((<[次を検索]|URL:EditFindNextAction.html>))
直前の検索条件で次を検索します。


+((<[前を検索]|URL:EditFindPrevAction.html>))
直前の検索条件で前を検索します。


+((<[置換]|URL:EditReplaceAction.html>))
テキストを置換します。

=end
