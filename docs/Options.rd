=begin
=オプションの設定

QMAIL3全体の設定は[オプション]ダイアログで行います。このダイアログでは左側のリスト（Pocket PC版では上のコンボボックス）で項目を選択すると、右側（Pocket PC版では下側）のパネルが切り替わり、各項目の設定が行えます。以下のパネルがあります。

*((<フォルダビューの設定|URL:OptionFolder.html>))
*((<リストビューの設定|URL:OptionList.html>))
*((<プレビューの設定|URL:OptionPreview.html>))
*((<メッセージビューの設定|URL:OptionMessage.html>))
*((<ヘッダビューの設定|URL:OptionHeader.html>))
*((<エディットビューの設定|URL:OptionEdit.html>))
*((<エディットビュー2の設定|URL:OptionEdit2.html>))
*((<タブの設定|URL:OptionTab.html>))
*((<アドレス帳の設定|URL:OptionAddressBook.html>))
*((<振り分けの設定|URL:OptionRules.html>))
*((<色の設定|URL:OptionColors.html>))
*((<巡回の設定|URL:OptionGoRound.html>))
*((<署名の設定|URL:OptionSignatures.html>))
*((<定型文の設定|URL:OptionTexts.html>))
*((<フィルタの設定|URL:OptionFilters.html>))
*((<同期フィルタの設定|URL:OptionSyncFilters.html>))
*((<自動巡回の設定|URL:OptionAutoPilot.html>))
*((<同期の設定|URL:OptionSync.html>))
*((<検索の設定|URL:OptionSearch.html>))
*((<スパムフィルタの設定|URL:OptionJunkFilter.html>))
*((<セキュリティの設定|URL:OptionSecurity.html>))
*((<確認の設定|URL:OptionConfirm.html>))
*((<その他の設定|URL:OptionMisc.html>))
*((<その他2の設定|URL:OptionMisc2.html>))

=end
