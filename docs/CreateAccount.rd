=begin
=アカウントの作成

アカウントを作成するには、((<[アカウント]ダイアログ|URL:AccountDialog.html>))で[アカウント追加]ボタンをクリックします。すると、((<[アカウントの作成]ダイアログ|URL:CreateAccountDialog.html>))が開きます。このダイアログで必要な項目を指定して[OK]をクリックすると、引き続き((<アカウントのプロパティ|URL:AccountProperty.html>))を指定するためのダイアログが開きます。

=end
