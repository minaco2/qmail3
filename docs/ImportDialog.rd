=begin
=[読み込み]ダイアログ

((<[読み込み]ダイアログ|"IMG:images/ImportDialog.png">))


+[パス]
読み込むファイルを指定します。複数のファイルを指定する場合には、「;」で区切ります。ファイル名の部分にのみワイルドカード（*と?）が使えます。


+[参照]
読み込むファイルを指定するためのファイル選択ダイアログを開きます。


+[複数のメッセージを含む]
読み込むファイルが複数のメッセージを含む場合にチェックします。複数のメッセージを含む場合にはmbox形式である必要があります。チェックしない場合、一つのファイルに一つのメッセージが((<"message/rfc822形式"|URL:http://www.ietf.org/rfc/rfc2046.txt>))で保存されている必要があります。


+[エンコーディング]
特定のエンコーディングでファイルを読みたい場合に指定します。指定しない場合、各MIMEパートに対してContent-Typeで指定されたエンコーディングが使用されます。指定されると、Content-Typeの指定を無視して指定されたエンコーディングで読み込みます。QMAIL2やQMAIL3からエクスポートしたメッセージをインポートする場合には指定する必要はありません。


====[フラグ]
メッセージにX-QMAIL-Flagsヘッダが付いていた場合にどのように処理するかを指定します。[通常]または[QMAIL 2.0互換]を指定すると、X-QMAIL-Flagsで指定されたフラグがインポートされたメッセージに適用されます。


+[通常]
QMAIL3でエクスポートしたときに付加されたとみなして処理します。


+[QMAIL 2.0互換]
QMAIL2でエクスポートしたときに付加されたとみなして処理します。


+[無視]
無視します。

=end
