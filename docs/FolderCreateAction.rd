=begin
=FolderCreateアクション

新しいフォルダを作成します。新しいフォルダは現在選択されているフォルダの子フォルダとして作成されます。アカウントが選択されている場合にはルートにフォルダが作成されます。((<[フォルダの作成]ダイアログ|URL:CreateFolderDialog.html>))が開きます。

検索フォルダを作成した場合、その後、((<[検索条件]タブ|URL:FolderConditionPage.html>))で検索条件を指定します。

NNTPで購読するグループを追加する場合や、RSSで購読するフィードを追加する場合には、((<FolderSubscribeアクション|URL:FolderSubscribeAction.html>))を使用します。


==引数
なし


==有効なウィンドウ・ビュー
*メインウィンドウ

=end
