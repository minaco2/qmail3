=begin
=ToolAddAddressアクション

対象のメッセージの差出人をアドレス帳に追加します。実行すると((<[メールアドレスの追加]ダイアログ|URL:AddAddressDialog.html>))が開きます。

<<<focusedMessage.rd


==引数
なし


==有効なウィンドウ・ビュー
*リストビュー
*プレビュー
*メッセージビュー

=end
