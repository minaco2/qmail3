=begin
=FolderPropertyアクション

現在選択されているフォルダのプロパティを表示します。((<フォルダのプロパティ|URL:FolderProperty.html>))を参照してください。


==引数
なし


==有効なウィンドウ・ビュー
*メインウィンドウ

=end
