=begin
=FileSaveアクション

メインウィンドウでは保存されていない全てのデータをファイルに保存します。引数は無視されます。

エディットウィンドウではメッセージをmessage/rfc822の形式で保存します。引数を指定しないと、保存先のファイルを指定するためにファイルダイアログが開きます。

アドレス帳ウィンドウでは変更されたアドレス帳を保存します。引数は無視されます。


==引数
:1
  保存先のファイルのパス


==有効なウィンドウ・ビュー
*メインウィンドウ
*エディットウィンドウ
*アドレス帳ウィンドウ

=end
