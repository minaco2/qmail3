=begin
=定型文の設定

[オプション]ダイアログの[定型文]パネルでは((<定型文|URL:FixedFormText.html>))の設定を行います。

((<定型文の設定|"IMG:images/OptionFixedFormTexts.png">))


+[追加]
定型文を追加します。((<[定型文]ダイアログ|URL:FixedFormTextDialog.html>))が開きます。


+[削除]
選択された定型文を削除します。


+[編集]
選択された定型文を編集します。((<[定型文]ダイアログ|URL:FixedFormTextDialog.html>))が開きます。


+[上へ]
選択された定型文をひとつ上に移動します。


+[下へ]
選択された定型文をひとつ下に移動します。

=end
