=begin
=添付ファイルのコンテキストメニュー

((<添付ファイルのコンテキストメニュー|"IMG:images/AttachmentMenu.png">))


+((<[開く]|URL:AttachmentOpenAction.html>))
選択された添付ファイルを関連付けされたアプリケーションで開きます。


+((<[保存]|URL:AttachmentSaveAction.html>))
選択された添付ファイルを保存します。


+((<[全て保存]|URL:AttachmentSaveAllAction.html>))
全ての添付ファイルを保存します。

=end
