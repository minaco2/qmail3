=begin
=ViewRawModeアクション

((<メッセージ表示モード|URL:MessageViewMode.html>))の生メッセージモードをOn/Offします。


==引数
なし


==有効なウィンドウ・ビュー
*メインウィンドウ
*メッセージウィンドウ

=end
