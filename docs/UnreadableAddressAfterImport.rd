=begin
=メッセージをインポートすると送信者や宛先などが化けます

他のメーラがエクスポートしたメッセージを((<インポート|URL:ImportAndExport.html>))すると、送信者や宛先などが文字化けすることがあります。これはエクスポートするときにこれらの情報が正しくエンコードされていないためです。

たとえば、宛先が「テスト <test@example.org>」の場合、

 To: =?iso-2022-jp?B?GyRCJUYlOSVIGyhC?= <test@example.org>

のようになっている必要がありますが、メーラによっては、

 To: テスト <test@example.org>

のように日本語を直接出力します。

このようなメッセージを読み込むと、この部分が文字化けします。これを回避するには、((<インポートダイアログ|URL:ImportDialog.html>))の[エンコーディング]で適切なエンコーディング（日本語の場合にはほとんどの場合、ISO-2022-JP）を指定してください。ただし、エンコーディングを指定すると、Content-Typeに指定された文字コードを無視して指定されたエンコーディングで変換を行うため、日本語と英語以外の文字が含まれているとその部分が文字化けする可能性があります。

Subjectなどの構造化ヘッダは、日本語が直接出力されていても、それがISO-2022-JPでエンコーディングされていて、インポートする環境が日本語環境ならば、正しく扱えます。しかし、ToやFromのような構造化ヘッダでは、ISO-2022-JPのエスケープシーケンスに含まれる「(」がコメントの始まりと区別が付かないため、エンコーディングを指定しないと正しく扱えません。

=end
