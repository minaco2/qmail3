=begin
=MessageMarkDeletedアクション

対象のメッセージの削除フラグを立てます。

<<<selectedMessage.rd


==引数
なし


==有効なウィンドウ・ビュー
*リストビュー
*プレビュー
*メッセージウィンドウ

=end
