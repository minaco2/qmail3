=begin
=FolderEmptyアクション

引数で指定されたフォルダの全てのメッセージを削除します。引数が指定されていない場合には、現在選択されているフォルダの全てのメッセージを削除します。そのアカウントにゴミ箱（ゴミ箱フラグの立ったフォルダ）がある場合には、そのフォルダにメッセージを移動し、ない場合には直接メッセージを削除します。

ただし、対象のメッセージがゴミ箱またはスパムフォルダ（スパムフラグの立ったフォルダ）にある場合、ゴミ箱の有無に関わらず直接メッセージを削除します。

引数でフォルダを指定するときには、以下の形式のいずれかが使用できます。

<<<folderName.rd

指定されたフォルダが見つからない場合には何もしません。


==引数
:1
  フォルダ


==有効なウィンドウ・ビュー
*メインウィンドウ

=end
