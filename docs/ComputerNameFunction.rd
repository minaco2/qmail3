=begin
=@ComputerName

 String @ComputerName()


==説明
実行しているPCの名前を返します。取得できなかった場合には空文字列を返します。


==引数
なし


==エラー
*引数の数が合っていない場合


==条件
なし


==例
 # PC名を取得
 @ComputerName()

=end
