=begin
=MessageApplyRuleアクション

現在選択されているフォルダの表示されているすべてのメッセージを振り分けます。((<フィルタ|URL:Filter.html>))によって除外されているメッセージは振り分け対象になりません。振り分けについては、((<振り分け|URL:ApplyRules.html>))を参照してください。


==引数
なし


==有効なウィンドウ・ビュー
*メインウィンドウ

=end
