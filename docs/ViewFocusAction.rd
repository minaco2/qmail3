=begin
=ViewFocusアクション

引数で指定されたビューにフォーカスを移動します。

指定できるビューの名前は以下の通りです。

:Folder
  フォルダビュー
:FolderComboBox
  フォルダコンボボックス
:FolderList
  フォルダリストビュー
:List
  リストビュー
:Preview
  プレビュー


==引数
:1
  ビューの名前

==有効なウィンドウ・ビュー
*メインウィンドウ

=end
