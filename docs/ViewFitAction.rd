=begin
=ViewFitアクション

HTML表示の配置方法を指定します。Pocket PC版のみで有効です。指定できるのは、none, normal, superで、後ろのものほど一列で表示するように配置されます。引数を指定しない場合、noneと同様に振舞います。


==引数
:1
  配置方法。none, normal, superにいずれか


==有効なウィンドウ・ビュー
*プレビュー
*メッセージビュー

=end
