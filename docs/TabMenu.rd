=begin
=タブのコンテキストメニュー

((<タブのコンテキストメニュー|"IMG:images/TabMenu.png">))


+((<[閉じる]|URL:TabCloseAction.html>))
タブを閉じます。


+((<[タイトルを編集]|URL:TabEditTitleAction.html>))
タブのタイトルを編集します。


+((<[ロック]|URL:TabLockAction.html>))
タブをロックします。

=end
