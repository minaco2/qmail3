=begin
=MessagePropertyアクション

対象メッセージのプロパティを表示します。((<メッセージのプロパティ|URL:MessageProperty.html>))を参照してください。

<<<selectedMessage.rd


==引数
なし


==有効なウィンドウ・ビュー
*リストビュー
*プレビュー
*メッセージウィンドウ

=end
