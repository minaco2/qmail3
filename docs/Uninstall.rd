=begin
=アンインストール

==インストーラでインストールした場合

インストーラでインストールした場合には、スタートメニューのQMAIL3フォルダからUninstallを選択してアンインストーラを実行してください。

デフォルトではプログラムのみを削除してメールのデータは消さないようになっています。メールデータも消したい場合には、[メールボックスの削除]ページで[メールボックスを削除]にチェックを入れてください。


==Zipファイルでインストールし場合

Zipファイルでインストールした場合には以下の手順でアンインストールしてください。

(1)メニューから[ファイル]-[保守]-[アンインストール]を実行し、QMAIL3が使用していたレジストリ情報を削除します
(2)実行ファイルをコピーしたディレクトリを削除します
(3)メールデータも削除する場合にはメールボックスディレクトリを削除します

=end
