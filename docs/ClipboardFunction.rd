=begin
=@Clipboard

 String @Clipboard(String s?)


==説明
引数が指定されていない場合にはクリップボードから文字列を取得して返します。取得できない場合には空文字列を返します。

引数が指定された場合にはsをクリップボードにセットします。


==引数
:String s
  クリップボードにセットする文字列


==エラー
*引数の数が合っていない場合
*クリップボードにセットするのに失敗した場合


==条件
なし


==例
 # クリップボードから文字列を取得
 @Clipboard()
 
 # クリップボードに本文をコピー
 @Clipboard(@Body())

=end
