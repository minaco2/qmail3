=begin
=その他の設定

[オプション]ダイアログの[その他]パネルではその他の設定を行います。

((<その他の設定|"IMG:images/OptionMisc.png">))


+[ビューの配置]
メインウィンドウの分割方法と各ビューの配置方法を指定します。

Fはフォルダビュー、Lはリストビュー、Pはプレビューを意味します。|は横に分割、-は縦に分割を意味します。また、()で括られているほうがネストされた分割を意味します。

幾つか例を挙げると、

:F|(L-P)
 フォルダビューが左、右側を縦に分割して上がリストビュー、下がプレビュー
:F|(L|P)
 左からフォルダビュー、リストビュー、プレビューの順番で横に三分割
:(F|L)-P
 上に左からフォルダビュー、リストビュー、下にプレビュー
:F-(L-P)
 上からフォルダビュー、リストビュー、プレビューの順番で縦に三分割
:(F-L)|P
 左側に上からフォルダビュー、リストビューで、右にプレビュー

のようになります。

プレビューを非表示にして使わない場合などにも必ず三つのビューすべてを指定する必要があります。また、縦や横に三つ並べる場合でも必ず()を使ってどちらがネストされるかを指定する必要があります。


+[最後までスクロールしたら次の未読を表示]
メッセージビューやプレビューでスペースキーを押すと次のページへスクロールし、最後までスクロールすると次のメッセージを表示します。次のメッセージを表示する代わりに次の未読メッセージを表示するかどうかを指定します。デフォルトでは次のメッセージを表示します。


+[別のアカウント内の未読にもジャンプする]
((<[メッセージ]-[次の未読メッセージ]|URL:ViewNextUnseenMessageAction.html>))を選択したときに、他のアカウントにある未読メッセージにもジャンプするかどうかを指定します。デフォルトではジャンプしません。


+[終了時にゴミ箱を空にする]
QMAIL3を終了するときにゴミ箱を空にするかどうかを指定します。デフォルトでは空にしません。


+[ウィンドウが非アクティブになったら保存する]
メインウィンドウが非アクティブになったら未保存のデータを保存するかどうかを指定します。デフォルトでは保存します。


+[最小化されたから隠す]
ウィンドウが最小化されたときにタスクトレイ上にアイコンとして表示し、タスクバーのボタンとして表示しないようにするかどうかを指定します。デフォルトでは隠しません。


+[ようこそ画面に未読数を表示する]
Windows XPのようこそ画面に未読メッセージ数を表示するかどうかを指定します。デフォルトでは表示しません。


+[エンコーディング]
メニューから選択できるエンコーディングのリストを指定します。複数のエンコーディングを空白で区切って指定することができます。処理できるエンコーディングのリストはシステムにインストールされているエンコーディングによって決まるため、ここでの指定は関係ありません。ここでリストしなかったエンコーディングでもシステムにインストールされていれば処理できますし、逆にここでリストしてもインストールされていなければ処理できません。デフォルトは「iso-8859-1 iso-2022-jp shift_jis euc-jp utf-8」です。


+[デフォルト]
デフォルトのエンコーディングを指定します。「OSのデフォルト」を選択するとOSのデフォルトのインターネットにおけるエンコーディング（日本語の場合にはiso-2022-jp）が使用されます。

デフォルトのエンコーディングは、メッセージ作成時のデフォルトのエンコーディングや、非MIMEメッセージを処理するときのエンコーディングとして使われます。


+[ログレベル]
ログレベルを指定します。ログレベルは、NONE, FATAL, ERROR, WARN, INFO, DEBUGのいずれから指定します。後ろの方の値を指定すればするほど多くのログが出力されます。ログについては、((<ログ|URL:Log.html>))を参照してください。

=end
