=begin
=プレビュー・メッセージビューのコンテキストメニュー

((<プレビュー・メッセージビューのコンテキストメニュー|"IMG:images/MessageMenu.png">))


+((<[次のメッセージ]|URL:ViewNextMessageAction.html>))
ひとつ後のメッセージを表示します。


+((<[前のメッセージ]|URL:ViewPrevMessageAction.html>))
ひとつ前のメッセージを表示します。


+((<[次の未読メッセージ]|URL:ViewNextUnseenMessageAction.html>))
次の未読メッセージを表示します。


+((<[削除]|URL:EditDeleteAction.html>))
メッセージを削除します。


+((<[スパムとして削除]|URL:EditDeleteJunkAction.html>))
メッセージをスパムとして削除します。


+((<[コピー]|URL:EditCopyAction.html>))
メッセージをクリップボードにコピーします。


+((<[全て選択]|URL:EditSelectAllAction.html>))
テキストを全て選択します。


+((<[メッセージ内検索]|URL:EditFindAction.html>))
メッセージ内のテキストを検索します。


+[移動]

*((<[<フォルダ名>]|URL:MessageMoveAction.html>))
 
 メッセージを<フォルダ名>のフォルダに移動します。

*((<[その他]|URL:MessageMoveAction.html>))
 
 メッセージをダイアログで指定したフォルダに移動します。


+[添付ファイル]

*((<[保存]|URL:MessageDetachAction.html>))
 
 添付ファイルを保存します。

*((<[削除]|URL:MessageDeleteAttachmentAction.html>))
 
 添付ファイルを削除します。

*((<[<添付ファイル名>]|URL:MessageOpenAttachmentAction.html>))
 
 <添付ファイル名>の添付ファイルを関連付けで開きます。

*((<[ダイジェストを展開]|URL:MessageExpandDigestAction.html>))
 
 メッセージがダイジェスト形式の場合に展開します。


+[マーク]

*((<[既読にする]|URL:MessageMarkSeenAction.html>))
 
 メッセージを既読にします。

*((<[未読にする]|URL:MessageUnmarkSeenAction.html>))
 
 メッセージを未読にします。

*((<[マークをつける]|URL:MessageMarkAction.html>))
 
 メッセージにマークをつけます。

*((<[マークを消す]|URL:MessageUnmarkAction.html>))
 
 メッセージのマークを消します。

*((<[ダウンロード予約]|URL:MessageMarkDownloadAction.html>))
 
 メッセージをダウンロード予約します。

*((<[本文をダウンロード予約]|URL:MessageMarkDownloadTextAction.html>))
 
 メッセージのテキスト部分をダウンロード予約します。

*((<[削除マークをつける]|URL:MessageMarkDeletedAction.html>))
 
 メッセージに削除マークをつけます。


+((<[ラベル]|URL:MessageLabelAction.html>))
メッセージのラベルを編集します。


+((<[プロパティ]|URL:MessagePropertyAction.html>))
メッセージのプロパティを表示します。

=end
