=begin
=リストビューのコンテキストメニュー

((<リストビューのコンテキストメニュー|"IMG:images/ListMenu.png">))


+((<[新規]|URL:MessageCreateAction.html>))
新規メッセージを作成します。


+((<[返信]|URL:MessageCreateAction.html>))
メッセージに返信します。


+((<[全員に返信]|URL:MessageCreateAction.html>))
メッセージを全員に返信します。


+((<[転送]|URL:MessageCreateAction.html>))
メッセージを転送します。


+((<[編集]|URL:MessageCreateAction.html>))
メッセージを編集します。


+[テンプレート]

*((<[<テンプレート名>]|URL:MessageCreateAction.html>))
 
 ((<作成用のテンプレート|URL:CreateTemplate.html>))を使用してメッセージを作成します。


+((<[削除]|URL:EditDeleteAction.html>))
メッセージを削除します。


+((<[スパムとして削除]|URL:EditDeleteJunkAction.html>))
メッセージをスパムとして削除します。


+((<[全て選択]|URL:EditSelectAllAction.html>))
全てのメッセージを選択します。


+[移動]

*((<[<フォルダ名>]|URL:MessageMoveAction.html>))
 
 メッセージを<フォルダ名>のフォルダに移動します。

*((<[その他]|URL:MessageMoveAction.html>))
 
 メッセージをダイアログで指定したフォルダに移動します。


+((<[ラベル]|URL:MessageLabelAction.html>))
メッセージのラベルを編集します。


+((<[選択されたメッセージを振り分け]|URL:MessageApplyRuleSelectedAction.html>))
メッセージを振り分けます。


+[添付ファイル]

*((<[保存]|URL:MessageDetachAction.html>))
 
 添付ファイルを保存します。

*((<[削除]|URL:MessageDeleteAttachmentAction.html>))
 
 添付ファイルを削除します。

*((<[<添付ファイル名>]|URL:MessageOpenAttachmentAction.html>))
 
 <添付ファイル名>の添付ファイルを関連付けで開きます。

*((<[ダイジェストを展開]|URL:MessageExpandDigestAction.html>))
 
 メッセージがダイジェスト形式の場合に展開します。

*((<[結合]|URL:MessageCombineAction.html>))
 
 分割されているメッセージを結合します。


+[マーク]

*((<[既読にする]|URL:MessageMarkSeenAction.html>))
 
 メッセージを既読にします。

*((<[未読にする]|URL:MessageUnmarkSeenAction.html>))
 
 メッセージを未読にします。

*((<[マークをつける]|URL:MessageMarkAction.html>))
 
 メッセージにマークをつけます。

*((<[マークを消す]|URL:MessageUnmarkAction.html>))
 
 メッセージのマークを消します。

*((<[ダウンロード予約]|URL:MessageMarkDownloadAction.html>))
 
 メッセージをダウンロード予約します。

*((<[本文をダウンロード予約]|URL:MessageMarkDownloadTextAction.html>))
 
 メッセージのテキスト部分をダウンロード予約します。

*((<[削除マークをつける]|URL:MessageMarkDeletedAction.html>))
 
 メッセージに削除マークをつけます。


+((<[プロパティ]|URL:MessagePropertyAction.html>))
メッセージのプロパティを表示します。

=end
