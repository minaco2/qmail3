=begin
=AddressCreateMessageアクション

引数で指定されたメールアドレス宛てのメッセージを作成します。メッセージの作成には、mailto URLの関連付けを使用します。


==引数
:1
  メールアドレス


==有効なウィンドウ・ビュー
*アドレス帳ウィンドウ

=end
