=begin
=画面構成

((<全てのウィンドウ|"IMG:images/AllWindows.png">))

各ウィンドウについては以下を参照してください。

*((<メインウィンドウ|URL:MainWindow.html>))
*((<メッセージウィンドウ|URL:MessageWindow.html>))
*((<エディットウィンドウ|URL:EditWindow.html>))
*((<アドレス帳ウィンドウ|URL:AddressBookWindow.html>))
*((<同期ダイアログ|URL:SyncDialog.html>))

=end
