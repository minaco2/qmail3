=begin
=MessageExpandDigestアクション

ダイジェスト形式のメッセージを分割します。対応しているダイジェスト形式は、((<"multipart/digest形式"|URL:http://www.ietf.org/rfc/rfc2046.txt>))と、((<RFC1153形式|URL:http://www.ietf.org/rfc/rfc1153.txt>))です。分割されたメッセージは、元のメッセージと同じフォルダに追加されます。


==引数
なし


==有効なウィンドウ・ビュー
*リストビュー
*プレビュー
*メッセージビュー

=end
