=begin
=MessageApplyRuleBackgroundアクション

現在選択されているフォルダの表示されているすべてのメッセージをバックグラウンドで振り分けます。振り分けについては、((<振り分け|URL:ApplyRules.html>))を参照してください。


==引数
なし


==有効なウィンドウ・ビュー
*メインウィンドウ

=end
