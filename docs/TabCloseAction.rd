=begin
=TabCloseアクション

現在のタブを閉じます。他にタブがない場合には何もしません。


==引数
なし


==有効なウィンドウ・ビュー
*メインウィンドウ

=end
