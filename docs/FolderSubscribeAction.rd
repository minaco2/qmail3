=begin
=FolderSubscribeアクション

フォルダを購読します。動作はアカウントの受信プロトコルによって異なります。たとえば、NNTPでは購読するグループを追加し、RSSでは購読するフィードを追加します。

NNTPでの動作は((<グループの購読|URL:SubscribeNntpGroup.html>))を、RSSでの動作は((<フィードの購読|URL:SubscribeRssFeed.html>))を参照してください。


==引数
なし


==有効なウィンドウ・ビュー
*メインウィンドウ

=end
