=begin
=ToolPGPMimeアクション

((<PGP|URL:PGP.html>))で暗号化や署名をするときに、PGP/MIMEを使うかインラインのPGPを使うかを切り替えます。

デフォルトでどちらになるかは、((<エディットビュー2の設定|URL:OptionEdit2.html>))で設定します。

==引数
なし


==有効なウィンドウ・ビュー
*エディットウィンドウ

=end
