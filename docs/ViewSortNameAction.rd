=begin
=ViewSortNameアクション

エントリを名前でソートします。エントリにソートキーが指定されている場合にはソートキーが使われます。


==引数
なし


==有効なウィンドウ・ビュー
*アドレス帳ウィンドウ

=end
