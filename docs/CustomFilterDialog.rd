=begin
=[カスタムフィルタ]ダイアログ

カスタムフィルタを指定します。

((<[カスタムフィルタ]ダイアログ|"IMG:images/CustomFilterDialog.png">))


+[条件]
カスタムフィルタの条件をマクロで指定します。


+[編集]
((<[条件]ダイアログ|URL:ConditionsDialog.html>))を開いて条件を編集します。

=end
