=begin
=確認の設定

[オプション]ダイアログの[確認]パネルでは、操作したときに確認をするかどうかの設定を行います。

((<確認の設定|"IMG:images/OptionConfirm.png">))


+[メッセージを削除する前に確認する]
メッセージを削除する前に確認するかどうかを指定します。デフォルトでは確認しません。


+[フォルダを空にする前に確認する]
((<フォルダを空にする|URL:FolderEmptyAction.html>))前に確認するかどうかを指定します。デフォルトでは確認します。


+[ゴミ箱を空にする前に確認する]
((<ゴミ箱を空にする|URL:FolderEmptyTrashAction.html>))前に確認するかどうかを指定します。デフォルトでは確認します。

=end
