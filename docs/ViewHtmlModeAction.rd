=begin
=ViewHtmlModeアクション

((<メッセージ表示モード|URL:MessageViewMode.html>))のHTML表示モードをOn/Offします。


==引数
なし


==有効なウィンドウ・ビュー
*メインウィンドウ
*メッセージウィンドウ

=end
