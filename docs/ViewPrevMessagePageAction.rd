=begin
=ViewPrevMessagePageアクション

現在表示しているメッセージを前のページにスクロールします。すでに最初までスクロールしている場合には、((<ViewPrevMessageアクション|URL:ViewPrevMessageAction.html>))と同様に振舞います。

=end
