=begin
=TabEditTitleアクション

現在のタブのタイトルを編集します。

実行すると((<[タイトル]ダイアログ|URL:TabTitleDialog.html>))が開きます。


==引数
なし


==有効なウィンドウ・ビュー
*メインウィンドウ

=end
