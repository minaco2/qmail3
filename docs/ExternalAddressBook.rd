=begin
=外部アドレス帳

外部アドレス帳を取り込むと、送信先のメールアドレスの((<選択|URL:ToolSelectAddressAction.html>))や((<自動補完|URL:AddressAutoComplete.html>))などで使用することができます。取り込めるアドレス帳は、Windows版ではWindowsアドレス帳とOutlookのアドレス帳、Windows CE版ではPocket Outlookのアドレス帳です。どの外部アドレス帳を取り込むかは、((<アドレス帳の設定|URL:OptionAddressBook.html>))で指定します。

=end
