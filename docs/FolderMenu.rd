=begin
=フォルダビュー・フォルダコンボボックスのコンテキストメニュー

((<フォルダビュー・フォルダコンボボックスのコンテキストメニュー|"IMG:images/FolderMenu.png">))


+((<[新しいタブで開く]|URL:TabCreateAction.html>))
選択されたフォルダを新しいタブで開きます。


+((<[作成]|URL:FolderCreateAction.html>))
新しいフォルダを作成します。


+((<[削除]|URL:FolderDeleteAction.html>))
選択されたフォルダを削除します。


+((<[名前の変更]|URL:FolderRenameAction.html>))
選択されたフォルダの名前を変更します。


+((<[フォルダリストを更新]|URL:FolderUpdateAction.html>))
IMAP4アカウントでフォルダリストを更新します。


+((<[購読]|URL:FolderSubscribeAction.html>))
RSSアカウントで((<新しいフィードを購読|URL:SubscribeRssFeed.html>))したり、NNTPアカウントで((<新しいグループを購読|URL:SubscribeNntpGroup.html>))します。


+((<[空にする]|URL:FolderEmptyAction.html>))
選択されたフォルダを空にします。


+((<[ゴミ箱を空にする]|URL:FolderEmptyTrashAction.html>))
ゴミ箱を空にします。


+((<[プロパティ]|URL:FolderPropertyAction.html>))
((<フォルダのプロパティ|URL:FolderProperty.html>))を表示します。

=end
