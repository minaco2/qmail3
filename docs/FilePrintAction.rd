=begin
=FilePrintアクション

対象のメッセージを印刷します。印刷の詳細は、((<印刷|URL:Printing.html>))を参照してください。

<<<selectedMessage.rd


==引数
なし


==有効なウィンドウ・ビュー
*リストビュー
*プレビュー
*メッセージウィンドウ

=end
