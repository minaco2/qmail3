=begin
=MessageApplyRuleSelectedアクション

選択されたメッセージを振り分けます。振り分けについては、((<振り分け|URL:ApplyRules.html>))を参照してください。

<<<selectedMessage.rd


==引数
なし


==有効なウィンドウ・ビュー
*メインウィンドウ

=end
