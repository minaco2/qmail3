=begin
=ダウンロード

==スナップショット版
スナップショット版はある程度安定しているバージョンです。ダウンロードする前に、((<変更履歴|URL:releasenotes.html>))を参照してください。インストールやバージョンアップの方法は、((<ドキュメント|URL:/doc/>))を参照してください。

現在のバージョンは、3.0.9です。

((:<table>
<tr>
<th>プラットフォーム</th>
<th>CPU</th>
<th>インストーラ</th>
<th>Zip</th>
</tr>
<tr>
<td>Windows 7/Vista/XP/2000</td>
<td>x86</td>
<td><a href="/q3/nph-snapshot.cgi?q3u-win-x86-ja.exe">インストーラ</a></td>
<td><a href="/q3/nph-snapshot.cgi?q3u-win-x86-ja.zip">Zip</a></td>
</tr>
<tr>
<td>Windows 7/Vista/XP</td>
<td>x64</td>
<td><a href="/q3/nph-snapshot.cgi?q3u-win-x64-ja.exe">インストーラ</a></td>
<td><a href="/q3/nph-snapshot.cgi?q3u-win-x64-ja.zip">Zip</a></td>
</tr>
<tr>
<td>Windows Mobile 6 Professional/Classic,<br>Windows Mobile 5.0</td>
<td>armv4i</td>
<td></td>
<td><a href="/q3/nph-snapshot.cgi?q3u-wm5-armv4i-ja.zip">Zip</a></td>
</tr>
</table>:))


==Nightly Build
Nightly Buildは自動的に作成されるバージョンで、不安定なこともあります。

((:<table>
<tr>
<th>プラットフォーム</th>
<th>CPU</th>
<th>インストーラ</th>
<th>Zip</th>
</tr>
<tr>
<td>Windows 7/Vista/XP/2000</td>
<td>x86</td>
<td><a href="/q3/nph-nightly.cgi?q3u-win-x86-ja.exe">インストーラ</a></td>
<td><a href="/q3/nph-nightly.cgi?q3u-win-x86-ja.zip">Zip</a></td>
</tr>
<tr>
<td>Windows 7/Vista/XP</td>
<td>x64</td>
<td><a href="/q3/nph-nightly.cgi?q3u-win-x64-ja.exe">インストーラ</a></td>
<td><a href="/q3/nph-nightly.cgi?q3u-win-x64-ja.zip">Zip</a></td>
</tr>
<tr>
<td>Windows Mobile 6 Professional/Classic,<br>Windows Mobile 5.0</td>
<td>armv4i</td>
<td></td>
<td><a href="/q3/nph-nightly.cgi?q3u-wm5-armv4i-ja.zip">Zip</a></td>
</tr>
<tr>
<td>Windows Mobile 6 Standard</td>
<td>armv4i</td>
<td></td>
<td><a href="/q3/nph-nightly.cgi?q3u-wm6std-armv4i-ja.zip">Zip</a></td>
</tr>
</table>:))


==((<古いバージョン|URL:old.html>))


=end
