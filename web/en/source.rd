=begin
=Source Code

Source code is available via Subversion. You can get it with the following command:

 svn co https://subversion.assembla.com/svn/q3/trunk

To build, you need Visual Studio 2005 Service Pack 1 with SDK as well as Cygwin or MinGW+MSYS.

=end
