=begin
=Old Versions
Followings are the versions for those platforms which are currently not supported.

((:<table>
<tr>
<th>Platform</th>
<th>CPU</th>
<th>Installer</th>
<th>Zip</th>
<th>Japanese UI</th>
</tr>
<tr>
<td>Windows 95/98/98SE/Me/NT 4.0</td>
<td>x86</td>
<td><a href="/download/snapshot/q3-win-x86-ja-3_0_0.exe">Installer</a></td>
<td><a href="/download/snapshot/q3-win-x86-ja-3_0_0.zip">Zip</a></td>
<td><a href="/download/snapshot/q3-win-x86-ja-mui0411-3_0_0.zip">Zip</a></td>
</tr>
<tr>
<td>Windows Mobile 2003 SE</td>
<td>armv4</td>
<td></td>
<td><a href="/download/snapshot/q3u-ppc2003se-armv4-ja-3_0_0.zip">Zip</a></td>
<td><a href="/download/snapshot/q3u-ppc2003se-armv4-ja-mui0411-3_0_0.zip">Zip</a></td>
</tr>
<tr>
<td>Windows Mobile 2003</td>
<td>armv4</td>
<td></td>
<td><a href="/download/snapshot/q3u-ppc2003-armv4-ja-3_0_0.zip">Zip</a></td>
<td><a href="/download/snapshot/q3u-ppc2003-armv4-ja-mui0411-3_0_0.zip">Zip</a></td>
</tr>
<tr>
<td>Sigmarion III</td>
<td>armv4i</td>
<td></td>
<td><a href="/download/snapshot/q3u-sig3-armv4i-ja-3_0_0.zip">Zip</a></td>
<td><a href="/download/snapshot/q3u-sig3-armv4i-ja-mui0411-3_0_0.zip">Zip</a></td>
</tr>
<tr>
<td>Pocket PC 2002</td>
<td>arm</td>
<td></td>
<td><a href="/download/snapshot/q3u-ppc2002-arm-ja-3_0_0.zip">Zip</a></td>
<td><a href="/download/snapshot/q3u-ppc2002-arm-ja-mui0411-3_0_0.zip">Zip</a></td>
</tr>
<tr>
<td>Pocket PC</td>
<td>arm</td>
<td></td>
<td><a href="/download/snapshot/q3u-ppc-arm-ja-3_0_0.zip">Zip</a></td>
<td><a href="/download/snapshot/q3u-ppc-arm-ja-mui0411-3_0_0.zip">Zip</a></td>
</tr>
<tr>
<td>Pocket PC</td>
<td>mips</td>
<td></td>
<td><a href="/download/snapshot/q3u-ppc-mips-ja-3_0_0.zip">Zip</a></td>
<td><a href="/download/snapshot/q3u-ppc-mips-ja-mui0411-3_0_0.zip">Zip</a></td>
</tr>
<tr>
<td>Pocket PC</td>
<td>sh3</td>
<td></td>
<td><a href="/download/snapshot/q3u-ppc-sh-ja-3_0_0.zip">Zip</a></td>
<td><a href="/download/snapshot/q3u-sh-arm-ja-mui0411-3_0_0.zip">Zip</a></td>
</tr>
<tr>
<td>HPC2000</td>
<td>arm</td>
<td></td>
<td><a href="/download/snapshot/q3u-hpc2000-arm-ja-3_0_0.zip">Zip</a></td>
<td><a href="/download/snapshot/q3u-hpc2000-arm-ja-mui0411-3_0_0.zip">Zip</a></td>
</tr>
<tr>
<td>HPC2000</td>
<td>mips</td>
<td></td>
<td><a href="/download/snapshot/q3u-hpc2000-mips-ja-3_0_0.zip">Zip</a></td>
<td><a href="/download/snapshot/q3u-hpc2000-mips-ja-mui0411-3_0_0.zip">Zip</a></td>
</tr>
<tr>
<td>HPC Pro</td>
<td>arm</td>
<td></td>
<td><a href="/download/snapshot/q3u-hpcpro-arm-ja-3_0_0.zip">Zip</a></td>
<td><a href="/download/snapshot/q3u-hpcpro-arm-ja-mui0411-3_0_0.zip">Zip</a></td>
</tr>
<tr>
<td>HPC Pro</td>
<td>mips</td>
<td></td>
<td><a href="/download/snapshot/q3u-hpcpro-mips-ja-3_0_0.zip">Zip</a></td>
<td><a href="/download/snapshot/q3u-hpcpro-mips-ja-mui0411-3_0_0.zip">Zip</a></td>
</tr>
<tr>
<td>HPC Pro</td>
<td>sh3</td>
<td></td>
<td><a href="/download/snapshot/q3u-hpcpro-sh3-ja-3_0_0.zip">Zip</a></td>
<td><a href="/download/snapshot/q3u-hpcpro-sh3-ja-mui0411-3_0_0.zip">Zip</a></td>
</tr>
<tr>
<td>HPC Pro</td>
<td>sh4</td>
<td></td>
<td><a href="/download/snapshot/q3u-hpcpro-sh4-ja-3_0_0.zip">Zip</a></td>
<td><a href="/download/snapshot/q3u-hpcpro-sh4-ja-mui0411-3_0_0.zip">Zip</a></td>
</tr>
</table>:))


==Documents
This is the documents at the time when these versions were released.

*((<Documents|URL:/download/snapshot/q3-doc-3_0_0.zip">))


==Runtime Library
You may need to install Microsoft Visual C++ 2005 SP1 Redistributable to run QMAIL3 on Windows.

*((<Microsoft Visual C++ 2005 SP1 Redistributable Package (x86)|URL:http://www.microsoft.com/downloads/details.aspx?displaylang=en&FamilyID=200b2fd9-ae1a-4a14-984d-389c36f85647>))
*((<Microsoft Visual C++ 2005 SP1 Redistributable Package (x64)|URL:http://www.microsoft.com/downloads/details.aspx?displaylang=en&FamilyID=eb4ebe2d-33c0-4a47-9dd4-b9a6d7bd44da>))

Or download the file below and put msvcr80.dll and Microsoft.VC80.CRT.manifest into the same directory where q3u.exe is installed.

((:<table>
<tr>
<th>Platform</th>
<th>CPU</th>
<th>Download</th>
</tr>
<tr>
<td>Windows</td>
<td>x86</td>
<td><a href="/download/crt/crt80sp1-x86.zip">Zip</a></td>
</tr>
<tr>
<td>Windows</td>
<td>x64</td>
<td><a href="/download/crt/crt80sp1-x64.zip">Zip</a></td>
</tr>
</table>:))


==OpenSSL
You may need OpenSSL libraries to use SSL and S/MIME. Though you can build from the source that you can down load from ((<OpenSSL page|URL:http://www.openssl.org>)), you can also download binaries from here. Currently OpenSSL 0.9.8e is available. Please download Windows CE version for Pocket PC 2002, HPC2000, Pocket PC or HPC Pro.

((:<table>
<tr>
<th>Platform</th>
<th>CPU</th>
<th>Download</th>
</tr>
<tr>
<td>Windows</td>
<td>x86</td>
<td><a href="/download/openssl/openssl-windows-x86-0.9.8e.zip">Zip</a></td>
</tr>
<tr>
<td>Windows</td>
<td>x64</td>
<td><a href="/download/openssl/openssl-windows-x64-0.9.8e.zip">Zip</a></td>
</tr>
<tr>
<td>Windows CE</td>
<td>arm</td>
<td><a href="/download/openssl/openssl-wince-arm-0.9.8e.zip">Zip</a></td>
</tr>
<tr>
<td>Windows CE</td>
<td>mips</td>
<td><a href="/download/openssl/openssl-wince-mips-0.9.8e.zip">Zip</a></td>
</tr>
<tr>
<td>Windows CE</td>
<td>sh3</td>
<td><a href="/download/openssl/openssl-wince-sh3-0.9.8e.zip">Zip</a></td>
</tr>
<tr>
<td>Windows CE</td>
<td>sh4</td>
<td><a href="/download/openssl/openssl-wince-sh4-0.9.8e.zip">Zip</a></td>
</tr>
</table>:))

The binary here doesn't work on Windows 95 or Windows NT 4.0 because VC8 runtime doesn't support those platforms. Please download libraries from ((<Win32 OpenSSL|URL:http://www.slproweb.com/products/Win32OpenSSL.html>)) instead.

=end
