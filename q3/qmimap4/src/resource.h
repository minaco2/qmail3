//{{NO_DEPENDENCIES}}
// Microsoft eMbedded Visual C++ generated include file.
// Used by qmimap4.rc
//
#define IDS_IMAP4                       1000
#define IDS_INITIALIZE                  2000
#define IDS_LOOKUP                      2001
#define IDS_CONNECTING                  2002
#define IDS_CONNECTED                   2003
#define IDS_AUTHENTICATING              2004
#define IDS_SELECTFOLDER                2005
#define IDS_CLOSEFOLDER                 2006
#define IDS_UPDATEMESSAGES              2007
#define IDS_DOWNLOADMESSAGESDATA        2008
#define IDS_SETFLAGS                    2009
#define IDS_DOWNLOADMESSAGES            2010
#define IDS_APPLYOFFLINEJOBS            2011
#define IDS_DOWNLOADRESERVEDMESSAGES    2012
#define IDS_IMAP4SEARCH                 2013
#define IDS_MANAGEJUNK                  2014
#define IDS_FILTERJUNK                  2015
#define IDS_ERROR_MESSAGE               10000
#define IDS_ERROR_GREETING              11000
#define IDS_ERROR_LOGIN                 11001
#define IDS_ERROR_CAPABILITY            11002
#define IDS_ERROR_FETCH                 11003
#define IDS_ERROR_STORE                 11004
#define IDS_ERROR_SELECT                11005
#define IDS_ERROR_LSUB                  11006
#define IDS_ERROR_LIST                  11007
#define IDS_ERROR_COPY                  11008
#define IDS_ERROR_APPEND                11009
#define IDS_ERROR_NOOP                  11010
#define IDS_ERROR_CREATE                11011
#define IDS_ERROR_DELETE                11012
#define IDS_ERROR_RENAME                11013
#define IDS_ERROR_SUBSCRIBE             11014
#define IDS_ERROR_UNSUBSCRIBE           11015
#define IDS_ERROR_CLOSE                 11016
#define IDS_ERROR_EXPUNGE               11017
#define IDS_ERROR_AUTHENTICATE          11018
#define IDS_ERROR_SEARCH                11019
#define IDS_ERROR_NAMESPACE             11020
#define IDS_ERROR_LOGOUT                11021
#define IDS_ERROR_STARTTLS              11022
#define IDS_ERROR_INITIALIZE            12000
#define IDS_ERROR_CONNECT               12001
#define IDS_ERROR_SELECTSOCKET          12002
#define IDS_ERROR_TIMEOUT               12003
#define IDS_ERROR_DISCONNECT            12004
#define IDS_ERROR_RECEIVE               12005
#define IDS_ERROR_PARSE                 12006
#define IDS_ERROR_OTHER                 12007
#define IDS_ERROR_INVALIDSOCKET         12008
#define IDS_ERROR_SEND                  12009
#define IDS_ERROR_RESPONSE              12010
#define IDS_ERROR_SSL                   12011
#define IDS_ERROR_SAVE                  14000
#define IDS_ERROR_APPLYRULES            14001
#define IDS_ERROR_MANAGEJUNK            14002
#define IDS_ERROR_FILTERJUNK            14003

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
