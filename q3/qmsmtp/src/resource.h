//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by qmsmtp.rc
//
#define IDS_SMTP                        100
#define IDS_INITIALIZE                  1000
#define IDS_LOOKUP                      1001
#define IDS_CONNECTING                  1002
#define IDS_CONNECTED                   1003
#define IDS_AUTHENTICATING              1004
#define IDS_SENDMESSAGE                 1005
#define IDS_ERROR_MESSAGE               10000
#define IDS_ERROR_GREETING              11000
#define IDS_ERROR_HELO                  11001
#define IDS_ERROR_EHLO                  11002
#define IDS_ERROR_AUTH                  11003
#define IDS_ERROR_MAIL                  11004
#define IDS_ERROR_RCPT                  11005
#define IDS_ERROR_DATA                  11006
#define IDS_ERROR_STARTTLS              11007
#define IDS_ERROR_RSET                  11008
#define IDS_ERROR_INITIALIZE            12000
#define IDS_ERROR_CONNECT               12001
#define IDS_ERROR_RESPONSE              12002
#define IDS_ERROR_INVALIDSOCKET         12003
#define IDS_ERROR_TIMEOUT               12004
#define IDS_ERROR_SELECT                12005
#define IDS_ERROR_DISCONNECT            12006
#define IDS_ERROR_RECEIVE               12007
#define IDS_ERROR_SEND                  12008
#define IDS_ERROR_OTHER                 12009
#define IDS_ERROR_SSL                   12010

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
