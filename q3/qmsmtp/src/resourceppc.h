//{{NO_DEPENDENCIES}}
// Microsoft eMbedded Visual C++ generated include file.
// Used by qmsmtpppc.rc
//
#define IDD_SEND                        101
#define IDD_POPBEFORESMTP               102
#define IDD_SEND_L                      601
#define IDD_POPBEFORESMTP_L             602
#define IDC_LOCALHOST                   1001
#define IDC_POPBEFORESMTP               1005
#define IDC_POPBEFORESMTPWAIT           1006
#define IDC_CUSTOM                      1007
#define IDC_DEFAULT                     1008
#define IDC_PROTOCOL                    1010
#define IDC_HOST                        1011
#define IDC_PORT                        1012
#define IDC_NONE                        1013
#define IDC_SSL                         1014
#define IDC_STARTTLS                    1015
#define IDC_APOP                        1016
#define IDC_LOCALHOSTLABEL              1017
#define IDC_POPBEFORESMTPWAITLABEL      1018
#define IDC_PROTOCOLLABEL               1019
#define IDC_HOSTLABEL                   1020
#define IDC_PORTLABEL                   1021
#define IDC_SECURE                      1022

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        103
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1023
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
