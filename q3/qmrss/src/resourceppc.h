//{{NO_DEPENDENCIES}}
// Microsoft eMbedded Visual C++ generated include file.
// Used by qmrssppc.rc
//
#define IDD_RECEIVE                     101
#define IDD_SEND                        102
#define IDD_SUBSCRIBEPROPERTY           103
#define IDD_SUBSCRIBEURL                104
#define IDD_RECEIVE_L                   601
#define IDD_SEND_L                      502
#define IDD_SUBSCRIBEPROPERTY_L         603
#define IDD_SUBSCRIBEURL_L              604
#define IDC_HOST                        1000
#define IDC_PORT                        1001
#define IDC_NOPROXY                     1004
#define IDC_INTERNETSETTING             1005
#define IDC_CUSTOM                      1006
#define IDC_URL                         1007
#define IDC_NAME                        1008
#define IDC_MAKEMULTIPART               1009
#define IDC_USEDESCRIPTIONASCONTENT     1010
#define IDC_UPDATEIFMODIFIED            1011
#define IDC_USERNAME                    1012
#define IDC_PASSWORD                    1013
#define IDC_AUTHENTICATE                1014
#define IDC_PROXY                       1015
#define IDC_HOSTLABEL                   1016
#define IDC_PORTLABEL                   1017
#define IDC_USERNAMELABEL               1018
#define IDC_PASSWORDLABEL               1019
#define IDC_NAMELABEL                   1020
#define IDC_URLLABEL                    1021

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        105
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1022
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
