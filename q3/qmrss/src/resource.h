//{{NO_DEPENDENCIES}}
// Microsoft eMbedded Visual C++ generated include file.
// Used by qmrss.rc
//
#define IDS_RSS                         1000
#define IDS_RSSSEND                     1001
#define IDS_FOLDER_TRASH                1100
#define IDS_INITIALIZE                  2000
#define IDS_LOOKUP                      2001
#define IDS_CONNECTING                  2002
#define IDS_CONNECTED                   2003
#define IDS_REQUESTRSS                  2004
#define IDS_PARSERSS                    2005
#define IDS_PROCESSRSS                  2006
#define IDS_SUBSCRIBE                   2100
#define IDS_CONTINUE                    2101
#define IDS_DUPLICATEDFEED              2102
#define IDS_ERROR_MESSAGE               10000
#define IDS_ERROR_URL                   11000
#define IDS_ERROR_GET                   11001
#define IDS_ERROR_PARSE                 11002
#define IDS_ERROR_APPLYRULES            11003
#define IDS_ERROR_EXCEEDMAXREDIRECT     11004
#define IDS_ERROR_PARSERESPONSEHEADER   11005
#define IDS_ERROR_PARSEREDIRECTLOCATION 11006
#define IDS_ERROR_INVALIDREDIRECTLOCATION 11007
#define IDS_ERROR_SUBSCRIBE             11008
#define IDS_ERROR_SAVE                  11009

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
