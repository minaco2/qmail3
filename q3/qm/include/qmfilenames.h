/*
 * $Id$
 *
 * Copyright(C) 1998-2008 Satoshi Nakamura
 *
 */

#ifndef __QMFILENAMES_H__
#define __QMFILENAMES_H__

#include <qm.h>

#include <qs.h>


namespace qm {

/****************************************************************************
 *
 * FileNames
 *
 */

struct FileNames
{
	static const WCHAR* ACCOUNT;
	static const WCHAR* ACCOUNT_XML;
	static const WCHAR* ADDRESSBOOK_XML;
	static const WCHAR* AUTOPILOT_XML;
	static const WCHAR* BOX_EXT;
	static const WCHAR* CA_PEM;
	static const WCHAR* CERT;
	static const WCHAR* CHECK;
	static const WCHAR* COLORS_XML;
	static const WCHAR* COMPACT;
	static const WCHAR* FILTERS_XML;
	static const WCHAR* FOLDER_BMP;
	static const WCHAR* FOLDERS_XML;
	static const WCHAR* FONTS_XML;
	static const WCHAR* GOROUND_XML;
	static const WCHAR* HEADER_XML;
	static const WCHAR* HEADEREDIT_XML;
	static const WCHAR* INDEX;
	static const WCHAR* INDEX_EXT;
	static const WCHAR* KEY;
	static const WCHAR* KEYMAP_XML;
	static const WCHAR* LIST_BMP;
	static const WCHAR* LISTDATA_BMP;
	static const WCHAR* MAP_EXT;
	static const WCHAR* MENUS_XML;
	static const WCHAR* MSG;
	static const WCHAR* NOTIFY_BMP;
	static const WCHAR* PASSWORDS_XML;
	static const WCHAR* PEM_EXT;
	static const WCHAR* QMAIL_XML;
	static const WCHAR* RESOURCES_XML;
	static const WCHAR* RULES_XML;
	static const WCHAR* SIGNATURES_XML;
	static const WCHAR* SYNCFILTERS_XML;
	static const WCHAR* TABS_XML;
	static const WCHAR* TEXTS_XML;
	static const WCHAR* TOOLBAR_BMP;
	static const WCHAR* TOOLBARS_XML;
	static const WCHAR* VIEW_XML;
	static const WCHAR* VIEWS;
	static const WCHAR* VIEWS_XML;
	static const WCHAR* XML_EXT;
};

};

#endif // __QMFILENAMES_H__
