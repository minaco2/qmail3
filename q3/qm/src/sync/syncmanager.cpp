/*
 * $Id$
 *
 * Copyright(C) 1998-2008 Satoshi Nakamura
 *
 */

#pragma warning(disable:4786)

#include <qmaccount.h>
#include <qmdocument.h>
#include <qmfilenames.h>
#include <qmfolder.h>
#include <qmjunk.h>
#include <qmmacro.h>
#include <qmmessage.h>
#include <qmmessageholder.h>
#include <qmrecents.h>
#include <qmsecurity.h>
#include <qmsession.h>
#include <qmsyncfilter.h>

#include <qsinit.h>
#include <qsstl.h>

#include <algorithm>
#include <functional>

#include <boost/bind.hpp>

#include "syncmanager.h"
#include "../main/main.h"
#include "../model/term.h"
#include "../model/uri.h"
#include "../ui/resourceinc.h"

using namespace qm;
using namespace qs;


/****************************************************************************
 *
 * SyncDataItem
 *
 */

qm::SyncDataItem::SyncDataItem(Type type,
							   Account* pAccount,
							   SubAccount* pSubAccount) :
	type_(type),
	pAccount_(pAccount),
	pSubAccount_(pSubAccount)
{
	assert(pAccount);
	assert(pSubAccount);
}

qm::SyncDataItem::~SyncDataItem()
{
}

SyncDataItem::Type qm::SyncDataItem::getType() const
{
	return type_;
}

Account* qm::SyncDataItem::getAccount() const
{
	return pAccount_;
}

SubAccount* qm::SyncDataItem::getSubAccount() const
{
	return pSubAccount_;
}


/****************************************************************************
 *
 * SyncItem
 *
 */

qm::SyncItem::SyncItem(Type type,
					   Account* pAccount,
					   SubAccount* pSubAccount) :
	SyncDataItem(type, pAccount, pSubAccount)
{
}

qm::SyncItem::~SyncItem()
{
}


/****************************************************************************
 *
 * ReceiveSyncItem
 *
 */

qm::ReceiveSyncItem::ReceiveSyncItem(Account* pAccount,
									 SubAccount* pSubAccount,
									 NormalFolder* pFolder,
									 std::auto_ptr<SyncFilterSet> pFilterSet,
									 unsigned int nFlags) :
	SyncItem(TYPE_RECEIVE, pAccount, pSubAccount),
	pFolder_(pFolder),
	pFilterSet_(pFilterSet),
	nFlags_(nFlags)
{
	assert(pFolder);
}

qm::ReceiveSyncItem::~ReceiveSyncItem()
{
}

const SyncItem* qm::ReceiveSyncItem::getSyncItem() const
{
	return this;
}

NormalFolder* qm::ReceiveSyncItem::getSyncFolder() const
{
	return pFolder_;
}

unsigned int qm::ReceiveSyncItem::getSelectFlags() const
{
	return (nFlags_ & FLAG_EMPTY ? ReceiveSession::SELECTFLAG_EMPTY : 0) |
		(nFlags_ & ReceiveSyncItem::FLAG_EXPUNGE ? ReceiveSession::SELECTFLAG_EXPUNGE : 0);
}

const SyncFilterSet* qm::ReceiveSyncItem::getSyncFilterSet() const
{
	return pFilterSet_.get();
}


/****************************************************************************
 *
 * SendSyncItem
 *
 */

qm::SendSyncItem::SendSyncItem(Account* pAccount,
							   SubAccount* pSubAccount,
							   const WCHAR* pwszMessageId) :
	SyncItem(TYPE_SEND, pAccount, pSubAccount),
	pOutbox_(0)
{
	NormalFolder* pOutbox = static_cast<NormalFolder*>(
		pAccount->getFolderByBoxFlag(Folder::FLAG_OUTBOX));
	if (pOutbox && pOutbox->isFlag(Folder::FLAG_SYNCABLE))
		pOutbox_ = pOutbox;
	
	if (pwszMessageId)
		wstrMessageId_ = allocWString(pwszMessageId);
}

qm::SendSyncItem::~SendSyncItem()
{
}

const WCHAR* qm::SendSyncItem::getMessageId() const
{
	return wstrMessageId_.get();
}

const SyncItem* qm::SendSyncItem::getSyncItem() const
{
	return pOutbox_ ? this : 0;
}

NormalFolder* qm::SendSyncItem::getSyncFolder() const
{
	return pOutbox_;
}

unsigned int qm::SendSyncItem::getSelectFlags() const
{
	return 0;
}

const SyncFilterSet* qm::SendSyncItem::getSyncFilterSet() const
{
	return 0;
}


/****************************************************************************
 *
 * ApplyRulesSyncDataItem
 *
 */

qm::ApplyRulesSyncDataItem::ApplyRulesSyncDataItem(Account* pAccount,
												   SubAccount* pSubAccount,
												   Folder* pFolder) :
	SyncDataItem(TYPE_APPLYRULES, pAccount, pSubAccount),
	pFolder_(pFolder)
{
	assert(pFolder);
}

qm::ApplyRulesSyncDataItem::~ApplyRulesSyncDataItem()
{
}

Folder* qm::ApplyRulesSyncDataItem::getFolder() const
{
	return pFolder_;
}

const SyncItem* qm::ApplyRulesSyncDataItem::getSyncItem() const
{
	return 0;
}


/****************************************************************************
 *
 * SyncDialup
 *
 */

qm::SyncDialup::SyncDialup(const WCHAR* pwszEntry,
						   unsigned int nFlags,
						   const WCHAR* pwszDialFrom,
						   unsigned int nDisconnectWait) :
	wstrEntry_(0),
	nFlags_(nFlags),
	wstrDialFrom_(0),
	nDisconnectWait_(nDisconnectWait)
{
	wstring_ptr wstrEntry;
	if (pwszEntry)
		wstrEntry = allocWString(pwszEntry);
	
	wstring_ptr wstrDialFrom;
	if (pwszDialFrom)
		wstrDialFrom = allocWString(pwszDialFrom);
	
	wstrEntry_ = wstrEntry;
	wstrDialFrom_ = wstrDialFrom;
}

qm::SyncDialup::~SyncDialup()
{
}

const WCHAR* qm::SyncDialup::getEntry() const
{
	return wstrEntry_.get();
}

unsigned int qm::SyncDialup::getFlags() const
{
	return nFlags_;
}

const WCHAR* qm::SyncDialup::getDialFrom() const
{
	return wstrDialFrom_.get();
}

unsigned int qm::SyncDialup::getDisconnectWait() const
{
	return nDisconnectWait_;
}


/****************************************************************************
 *
 * SyncData
 *
 */

qm::SyncData::SyncData(Document* pDocument,
					   Type type) :
	pDocument_(pDocument),
	type_(type),
	pCallback_(0)
{
}

qm::SyncData::~SyncData()
{
}

Document* qm::SyncData::getDocument() const
{
	return pDocument_;
}

SyncData::Type qm::SyncData::getType() const
{
	return type_;
}

const SyncDialup* qm::SyncData::getDialup() const
{
	return pDialup_.get();
}

SyncManagerCallback* qm::SyncData::getCallback() const
{
	return pCallback_;
}

void qm::SyncData::setDialup(std::auto_ptr<SyncDialup> pDialup)
{
	pDialup_ = pDialup;
}

void qm::SyncData::setCallback(SyncManagerCallback* pCallback)
{
	pCallback_ = pCallback;
}


/****************************************************************************
 *
 * StaticSyncData
 *
 */

qm::StaticSyncData::StaticSyncData(Document* pDocument,
								   Type type,
								   SyncManager* pManager) :
	SyncData(pDocument, type),
	pManager_(pManager),
	nSlot_(0)
{
}

qm::StaticSyncData::~StaticSyncData()
{
	std::for_each(listItem_.begin(), listItem_.end(),
		boost::bind(boost::checked_deleter<SyncDataItem>(),
			boost::bind(&SlotItemList::value_type::second, _1)));
}

void qm::StaticSyncData::getItems(ItemListList* pList)
{
	assert(pList);
	
	for (unsigned int nSlot = 0; nSlot <= nSlot_; ++nSlot) {
		ItemList listItem;
		for (SlotItemList::iterator it = listItem_.begin(); it != listItem_.end(); ++it) {
			if ((*it).first == nSlot)
				listItem.push_back((*it).second);
		}
		if (!listItem.empty())
			pList->push_back(listItem);
	}
	listItem_.clear();
	nSlot_ = 0;
}

bool qm::StaticSyncData::isEmpty() const
{
	return listItem_.empty();
}

void qm::StaticSyncData::newSlot()
{
	if (!listItem_.empty() && listItem_.back().first == nSlot_)
		++nSlot_;
}

void qm::StaticSyncData::addReceiveFolder(Account* pAccount,
										  SubAccount* pSubAccount,
										  NormalFolder* pFolder,
										  const WCHAR* pwszFilterName,
										  unsigned int nFlags)
{
	assert(pAccount);
	assert(pSubAccount);
	assert(pFolder);
	
	SyncFilterManager* pManager = pManager_->getSyncFilterManager();
	std::auto_ptr<SyncFilterSet> pFilterSet(pManager->getFilterSet(pwszFilterName));
	std::auto_ptr<ReceiveSyncItem> pItem(new ReceiveSyncItem(
		pAccount, pSubAccount, pFolder, pFilterSet, nFlags));
	listItem_.push_back(std::make_pair(nSlot_, pItem.get()));
	pItem.release();
}

void qm::StaticSyncData::addReceiveFolders(Account* pAccount,
										   SubAccount* pSubAccount,
										   const Term& folder,
										   const WCHAR* pwszFilterName)
{
	assert(pAccount);
	assert(pSubAccount);
	
	Account::NormalFolderList listFolder;
	
	NormalFolder* pTrash = static_cast<NormalFolder*>(
		pAccount->getFolderByBoxFlag(Folder::FLAG_TRASHBOX));
	if (pTrash && pTrash->isFlag(Folder::FLAG_SYNCABLE))
		pTrash = 0;
	
	const Account::FolderList& l = pAccount->getFolders();
	for (Account::FolderList::const_iterator it = l.begin(); it != l.end(); ++it) {
		Folder* pFolder = *it;
		if (pFolder->getType() == Folder::TYPE_NORMAL &&
			!pFolder->isFlag(Folder::FLAG_NOSELECT) &&
			!pFolder->isHidden() &&
			pFolder->isFlag(Folder::FLAG_SYNCABLE) &&
			(!pTrash || !pTrash->isAncestorOf(pFolder))) {
			if (!folder.isSpecified() || folder.match(pFolder->getFullName().get()))
				listFolder.push_back(static_cast<NormalFolder*>(pFolder));
		}
	}
	
	std::sort(listFolder.begin(), listFolder.end(), FolderLess());
	std::for_each(listFolder.begin(), listFolder.end(),
		boost::bind(&StaticSyncData::addReceiveFolder, this,
			pAccount, pSubAccount, _1, pwszFilterName, 0));
}

void qm::StaticSyncData::addSend(Account* pAccount,
								 SubAccount* pSubAccount,
								 const WCHAR* pwszMessageId)
{
	assert(pAccount);
	assert(pSubAccount);
	
	std::auto_ptr<SendSyncItem> pItem(new SendSyncItem(
		pAccount, pSubAccount, pwszMessageId));
	listItem_.push_back(std::make_pair(nSlot_, pItem.get()));
	pItem.release();
}

void qm::StaticSyncData::addApplyRulesFolder(Account* pAccount,
											 SubAccount* pSubAccount,
											 Folder* pFolder)
{
	assert(pAccount);
	assert(pSubAccount);
	assert(pFolder);
	
	std::auto_ptr<ApplyRulesSyncDataItem> pItem(
		new ApplyRulesSyncDataItem(pAccount, pSubAccount, pFolder));
	listItem_.push_back(std::make_pair(nSlot_, pItem.get()));
	pItem.release();
}

void qm::StaticSyncData::addApplyRulesFolders(Account* pAccount,
											  SubAccount* pSubAccount,
											  const Term& folder)
{
	assert(pAccount);
	assert(pSubAccount);
	
	Account::FolderList listFolder;
	
	NormalFolder* pTrash = static_cast<NormalFolder*>(
		pAccount->getFolderByBoxFlag(Folder::FLAG_TRASHBOX));
	
	const Account::FolderList& l = pAccount->getFolders();
	for (Account::FolderList::const_iterator it = l.begin(); it != l.end(); ++it) {
		Folder* pFolder = *it;
		if (!pFolder->isFlag(Folder::FLAG_NOSELECT) &&
			!pFolder->isHidden() &&
			(!pTrash || (pFolder != pTrash && !pTrash->isAncestorOf(pFolder)))) {
			if (!folder.isSpecified() || folder.match(pFolder->getFullName().get()))
				listFolder.push_back(pFolder);
		}
	}
	
	std::sort(listFolder.begin(), listFolder.end(), FolderLess());
	std::for_each(listFolder.begin(), listFolder.end(),
		boost::bind(&StaticSyncData::addApplyRulesFolder, this, pAccount, pSubAccount, _1));
}


/****************************************************************************
 *
 * SyncManager
 *
 */

qm::SyncManager::SyncManager(SyncFilterManager* pSyncFilterManager,
							 Profile* pProfile) :
	pSyncFilterManager_(pSyncFilterManager),
	pProfile_(pProfile),
	pSynchronizer_(InitThread::getInitThread().getSynchronizer()),
	nDialupConnectionCount_(0),
	bDialup_(false)
{
}

qm::SyncManager::~SyncManager()
{
	dispose();
}

void qm::SyncManager::dispose()
{
	typedef std::vector<HANDLE> Handles;
	Handles handles;
	{
		Lock<CriticalSection> lock(cs_);
		
		for (ThreadList::size_type n = 0; n < listThread_.size(); ++n) {
			SyncThread* pThread = listThread_[n];
			handles.push_back(pThread->getHandle());
			pThread->setWaitMode();
		}
	}
	
	if (!handles.empty()) {
		::WaitForMultipleObjects(static_cast<DWORD>(handles.size()),
			&handles[0], TRUE, INFINITE);
		std::for_each(listThread_.begin(), listThread_.end(),
			boost::checked_deleter<SyncThread>());
		listThread_.clear();
	}
	
	assert(listSyncingFolder_.empty());
	
	pProfile_ = 0;
}

bool qm::SyncManager::sync(std::auto_ptr<SyncData> pData)
{
	Lock<CriticalSection> lock(cs_);
	
	std::auto_ptr<SyncThread> pThread(new SyncThread(this, pData));
	if (!pThread->start())
		return false;
	listThread_.push_back(pThread.get());
	pThread.release();
	
	return true;
}

bool qm::SyncManager::isSyncing() const
{
	Lock<CriticalSection> lock(cs_);
	return !listThread_.empty();
}

SyncFilterManager* qm::SyncManager::getSyncFilterManager() const
{
	return pSyncFilterManager_;
}

void qm::SyncManager::addSyncManagerHandler(SyncManagerHandler* pHandler)
{
	listHandler_.push_back(pHandler);
}

void qm::SyncManager::removeSyncManagerHandler(SyncManagerHandler* pHandler)
{
	SyncManagerHandlerList::iterator it = std::remove(
		listHandler_.begin(), listHandler_.end(), pHandler);
	assert(it != listHandler_.end());
	listHandler_.erase(it, listHandler_.end());
}

void qm::SyncManager::fireStatusChanged(bool bStart)
{
	SyncManagerEvent event(this, bStart ? SyncManagerEvent::STATUS_START : SyncManagerEvent::STATUS_END);
	std::for_each(listHandler_.begin(), listHandler_.end(),
		boost::bind(&SyncManagerHandler::statusChanged, _1, boost::cref(event)));
}

void qm::SyncManager::addError(SyncManagerCallback* pCallback,
							   unsigned int nId,
							   Account* pAccount,
							   SubAccount* pSubAccount,
							   NormalFolder* pFolder,
							   UINT nMessageId,
							   const WCHAR* pwszDescription)
{
	wstring_ptr wstrMessage(loadString(getResourceHandle(), nMessageId));
	const WCHAR* pwszDescriptions[] = {
		pwszDescription
	};
	SessionErrorInfo info(pAccount, pSubAccount, pFolder,
		wstrMessage.get(), 0, pwszDescriptions, pwszDescription ? 1 : 0);
	pCallback->addError(nId, info);
}

bool qm::SyncManager::syncData(SyncData* pData)
{
	Log log(InitThread::getInitThread().getLogger(), L"qm::SyncManager::syncData");
	
	SyncManagerCallback* pCallback = pData->getCallback();
	assert(pCallback);
	
	struct CallbackCaller
	{
		CallbackCaller(SyncManagerCallback* pCallback,
					   const SyncData* pData) :
			pCallback_(pCallback)
		{
			pCallback_->start(pData->getType());
		}
		
		~CallbackCaller()
		{
			pCallback_->end();
		}
		
		SyncManagerCallback* pCallback_;
	} caller(pCallback, pData);
	
	const SyncDialup* pDialup = pData->getDialup();
	if (pDialup) {
		Lock<CriticalSection> lock(csDialup_);
		
		bool bDialup = !(pDialup->getFlags() & SyncDialup::FLAG_WHENEVERNOTCONNECTED) ||
			!RasConnection::isNetworkConnected();
		if (bDialup) {
			const WCHAR* pwszDialFrom = pDialup->getDialFrom();
			if (pwszDialFrom)
				RasConnection::setLocation(pwszDialFrom);
			
			const WCHAR* pwszEntry = pDialup->getEntry();
			wstring_ptr wstrEntry;
			if (!pwszEntry) {
				wstrEntry = pCallback->selectDialupEntry();
				if (!wstrEntry.get())
					return true;
				pwszEntry = wstrEntry.get();
			}
			assert(pwszEntry);
			
			RasConnectionCallbackImpl rasCallback(pDialup, pCallback);
			std::auto_ptr<RasConnection> pRasConnection(
				new RasConnection(&rasCallback));
			RasConnection::Result result = pRasConnection->connect(pwszEntry);
			if (result == RasConnection::RAS_FAIL)
				return false;
			pCallback->setMessage(-1, L"");
			if (result == RasConnection::RAS_CANCEL)
				return true;
		}
		++nDialupConnectionCount_;
		if (bDialup)
			bDialup_ = true;
	}
	
	struct DialupDisconnector
	{
		DialupDisconnector(const SyncDialup* pDialup,
						   LONG& nDialupConnectionCount,
						   bool& bDialup,
						   CriticalSection& cs) :
			pDialup_(pDialup),
			nDialupConnectionCount_(nDialupConnectionCount),
			bDialup_(bDialup),
			cs_(cs)
		{
		}
		
		~DialupDisconnector()
		{
			if (!pDialup_)
				return;
			
			Lock<CriticalSection> lock(cs_);
			
			if (--nDialupConnectionCount_ != 0 || !bDialup_)
				return;
			
			bDialup_ = false;
			
			if (pDialup_->getFlags() & SyncDialup::FLAG_NOTDISCONNECT)
				return;
			
			std::auto_ptr<RasConnection> pRasConnection(
				RasConnection::getActiveConnection(0));
			if (pRasConnection.get())
				pRasConnection->disconnect(pDialup_->getDisconnectWait());
		}
		
		const SyncDialup* pDialup_;
		LONG& nDialupConnectionCount_;
		bool& bDialup_;
		CriticalSection& cs_;
	} disconnector(pDialup, nDialupConnectionCount_, bDialup_, csDialup_);
	
	struct InternalOnline
	{
		InternalOnline(Document* pDocument,
					   Synchronizer* pSynchronizer) :
			pDocument_(pDocument),
			pSynchronizer_(pSynchronizer)
		{
			RunnableImpl runnable(pDocument_, true);
			pSynchronizer_->syncExec(&runnable);
		}
		
		~InternalOnline()
		{
			RunnableImpl runnable(pDocument_, false);
			pSynchronizer_->syncExec(&runnable);
		}
		
		struct RunnableImpl : public Runnable
		{
			RunnableImpl(Document* pDocument,
						 bool bIncrement) :
				pDocument_(pDocument),
				bIncrement_(bIncrement)
			{
			}
			
			virtual void run()
			{
				if (bIncrement_)
					pDocument_->incrementInternalOnline();
				else
					pDocument_->decrementInternalOnline();
			}
			
			Document* pDocument_;
			bool bIncrement_;
		};
		
		Document* pDocument_;
		Synchronizer* pSynchronizer_;
	} internalOnline(pData->getDocument(), pSynchronizer_);
	
	AccountManager::AccountList listAccount;
	while (true) {
		SyncData::ItemListList listItemList;
		struct Deleter
		{
			Deleter(SyncData::ItemListList& l) :
				l_(l)
			{
			}
			
			~Deleter()
			{
				for (SyncData::ItemListList::iterator it = l_.begin(); it != l_.end(); ++it) {
					SyncData::ItemList& l = *it;
					std::for_each(l.begin(), l.end(), boost::checked_deleter<SyncDataItem>());
				}
			}
			
			SyncData::ItemListList& l_;
		} deleter(listItemList);
		
		pData->getItems(&listItemList);
		if (listItemList.empty())
			break;
		
		for (SyncData::ItemListList::const_iterator it = listItemList.begin(); it != listItemList.end(); ++it) {
			const SyncData::ItemList& l = *it;
			std::transform(l.begin(), l.end(), std::back_inserter(listAccount),
				boost::bind(&SyncDataItem::getAccount, _1));
		}
		
		if (listItemList.size() == 1) {
			syncSlotData(pData, listItemList[0]);
		}
		else {
			typedef std::vector<Thread*> ThreadList;
			ThreadList listThread;
			listThread.reserve(listItemList.size());
			
			struct Wait
			{
				typedef std::vector<Thread*> ThreadList;
				
				Wait(const ThreadList& l) :
					l_(l)
				{
				}
				
				~Wait()
				{
					for (ThreadList::const_iterator it = l_.begin(); it != l_.end(); ++it) {
						std::auto_ptr<Thread> pThread(*it);
						pThread->join();
					}
				}
				
				const ThreadList& l_;
			} wait(listThread);
			
			for (SyncData::ItemListList::size_type n = 0; n < listItemList.size() - 1; ++n) {
				std::auto_ptr<ParallelSyncThread> pThread(new ParallelSyncThread(this, pData, listItemList[n]));
				if (!pThread->start())
					break;
				listThread.push_back(pThread.release());
			}
			syncSlotData(pData, listItemList.back());
		}
	}
	
	std::sort(listAccount.begin(), listAccount.end());
	AccountManager::AccountList::iterator itA = std::unique(
		listAccount.begin(), listAccount.end());
	for (AccountManager::AccountList::const_iterator it = listAccount.begin(); it != itA; ++it) {
		if (!(*it)->save(false))
			log.error(L"Failed to save an account.");
	}
	
	return true;
}

void qm::SyncManager::syncSlotData(const SyncData* pData,
								   const SyncData::ItemList& listItem)
{
	SyncManagerCallback* pCallback = pData->getCallback();
	assert(pCallback);
	
	unsigned int nId = ::GetCurrentThreadId();
	
	struct CallbackCaller
	{
		CallbackCaller(SyncManagerCallback* pCallback,
					   unsigned int nId,
					   SyncData::Type type) :
			pCallback_(pCallback),
			nId_(nId)
		{
			pCallback_->startThread(nId_, type);
		}
		
		~CallbackCaller()
		{
			pCallback_->endThread(nId_);
		}
		
		SyncManagerCallback* pCallback_;
		unsigned int nId_;
	} caller(pCallback, nId, pData->getType());
	
	struct ReceiveSessionTerm
	{
		ReceiveSessionTerm() :
			bConnected_(false)
		{
		}
		
		~ReceiveSessionTerm()
		{
			clear();
		}
		
		ReceiveSession* get() const
		{
			return pSession_.get();
		}
		
		void reset(std::auto_ptr<ReceiveSession> pSession)
		{
			if (pSession_.get()) {
				if (bConnected_)
					pSession_->disconnect();
				pSession_->term();
			}
			pSession_ = pSession;
			bConnected_ = false;
		}
		
		void clear()
		{
			std::auto_ptr<ReceiveSession> pSession;
			reset(pSession);
		}
		
		void setConnected()
		{
			bConnected_ = true;
		}
		
		std::auto_ptr<ReceiveSession> pSession_;
		bool bConnected_;
	};
	
	std::auto_ptr<Logger> pLogger;
	std::auto_ptr<ReceiveSessionCallback> pReceiveCallback;
	ReceiveSessionTerm session;
	SubAccount* pSubAccount = 0;
	typedef std::vector<SubAccount*> SubAccountList;
	SubAccountList listConnectFailed;
	for (SyncData::ItemList::const_iterator it = listItem.begin(); it != listItem.end(); ++it) {
		const SyncDataItem* pItem = *it;
		
		const SyncItem* pSyncItem = pItem->getSyncItem();
		if (pSyncItem) {
			if (pSubAccount != pSyncItem->getSubAccount() ||
				(session.get() && !session.get()->isConnected())) {
				pSubAccount = 0;
				session.clear();
				pReceiveCallback.reset(0);
				pLogger.reset(0);
				
				if (std::find(listConnectFailed.begin(), listConnectFailed.end(), pSyncItem->getSubAccount()) != listConnectFailed.end())
					continue;
				
				std::auto_ptr<ReceiveSession> pReceiveSession;
				if (!openReceiveSession(nId, pData->getDocument(),
					pCallback, pSyncItem, pData->getType(),
					&pReceiveSession, &pReceiveCallback, &pLogger))
					continue;
				session.reset(pReceiveSession);
				if (pCallback->isCanceled(nId, false))
					break;
				
				bool bConnect = session.get()->connect();
				if (pCallback->isCanceled(nId, false))
					break;
				if (!bConnect) {
					listConnectFailed.push_back(pSyncItem->getSubAccount());
					continue;
				}
				session.setConnected();
				
				if (!session.get()->applyOfflineJobs())
					continue;
				if (pCallback->isCanceled(nId, false))
					break;
				
				pSubAccount = pSyncItem->getSubAccount();
			}
			if (!syncFolder(nId, pData->getDocument(), pCallback, pSyncItem, session.get()))
				continue;
			if (pCallback->isCanceled(nId, false))
				break;
		}
		
		if (pItem->getType() == SyncItem::TYPE_SEND) {
			if (!send(nId, pData->getDocument(), pCallback, static_cast<const SendSyncItem*>(pItem)))
				continue;
		}
		else if (pItem->getType() == SyncDataItem::TYPE_APPLYRULES) {
			if (!applyRules(nId, pData->getDocument(), pCallback, static_cast<const ApplyRulesSyncDataItem*>(pItem)))
				continue;
		}
		if (pCallback->isCanceled(nId, false))
			break;
	}
}

bool qm::SyncManager::syncFolder(unsigned int nId,
								 Document* pDocument,
								 SyncManagerCallback* pSyncManagerCallback,
								 const SyncItem* pItem,
								 ReceiveSession* pSession)
{
	assert(pDocument);
	assert(pSyncManagerCallback);
	assert(pItem);
	assert(pSession);
	
	NormalFolder* pFolder = pItem->getSyncFolder();
	if (!pFolder || !pFolder->isFlag(Folder::FLAG_SYNCABLE))
		return true;
	
	pSyncManagerCallback->setFolder(nId, pFolder);
	
	FolderWait wait(this, pFolder);
	
	if (!pFolder->loadMessageHolders())
		return false;
	
	if (!pSession->selectFolder(pFolder, pItem->getSelectFlags()))
		return false;
	pFolder->setLastSyncTime(::GetTickCount());
	if (pSyncManagerCallback->isCanceled(nId, false))
		return true;
	
	if (!pSession->updateMessages())
		return false;
	if (pSyncManagerCallback->isCanceled(nId, false))
		return true;
	
	Account* pAccount = pItem->getAccount();
	SubAccount* pSubAccount = pItem->getSubAccount();
	bool bApplyRules = (pSubAccount->getAutoApplyRules() & SubAccount::AUTOAPPLYRULES_EXISTING) != 0;
	unsigned int nLastId = -1;
	if (bApplyRules) {
		Lock<Account> lock(*pAccount);
		if (!pFolder->loadMessageHolders())
			return false;
		unsigned int nCount = pFolder->getCount();
		if (nCount != 0)
			nLastId = pFolder->getMessage(nCount - 1)->getId();
	}
	
	if (!pSession->downloadMessages(pItem->getSyncFilterSet()))
		return false;
	if (pSyncManagerCallback->isCanceled(nId, false))
		return true;
	
	if (bApplyRules && nLastId != -1) {
		MessagePtrList l;
		{
			Lock<Account> lock(*pAccount);
			unsigned int nCount = pFolder->getCount();
			for (unsigned int n = 0; n < nCount; ++n) {
				MessageHolder* pmh = pFolder->getMessage(n);
				if (pmh->getId() > nLastId)
					break;
				l.push_back(MessagePtr(pmh));
			}
		}
		RuleManager* pRuleManager = pDocument->getRuleManager();
		RuleCallbackImpl callback(pSyncManagerCallback, nId);
		if (!pRuleManager->applyAuto(pFolder, &l, pSubAccount, pDocument,
			pProfile_, RuleManager::AUTOFLAG_EXISTING, &callback))
			return false;
	}
	
	if (!pAccount->saveMessages(false)) {
		addError(pSyncManagerCallback, nId, pAccount, 0, 0, IDS_ERROR_SAVE, 0);
		return false;
	}
	
	if (!pSession->closeFolder())
		return false;
	
	return true;
}

bool qm::SyncManager::send(unsigned int nId,
						   Document* pDocument,
						   SyncManagerCallback* pSyncManagerCallback,
						   const SendSyncItem* pItem)
{
	assert(pDocument);
	assert(pSyncManagerCallback);
	assert(pItem);
	
	Account* pAccount = pItem->getAccount();
	SubAccount* pSubAccount = pItem->getSubAccount();
	pSyncManagerCallback->setAccount(nId, pAccount, pSubAccount);
	
	NormalFolder* pOutbox = static_cast<NormalFolder*>(
		pAccount->getFolderByBoxFlag(Folder::FLAG_OUTBOX));
	if (!pOutbox) {
		addError(pSyncManagerCallback, nId, pAccount, 0, 0, IDS_ERROR_NOOUTBOX, 0);
		return false;
	}
	if (pOutbox->getFlags() & Folder::FLAG_BOX_MASK & ~Folder::FLAG_OUTBOX & ~Folder::FLAG_DRAFTBOX) {
		addError(pSyncManagerCallback, nId, pAccount, 0, 0, IDS_ERROR_OUTBOXHASEXTRAFLAGS, 0);
		return false;
	}
	if (!pOutbox->loadMessageHolders()) {
		addError(pSyncManagerCallback, nId, pAccount, 0, 0, IDS_ERROR_LOADOUTBOX, 0);
		return false;
	}
	
	NormalFolder* pSentbox = static_cast<NormalFolder*>(
		pAccount->getFolderByBoxFlag(Folder::FLAG_SENTBOX));
	if (!pSentbox) {
		addError(pSyncManagerCallback, nId, pAccount, 0, 0, IDS_ERROR_NOSENTBOX, 0);
		return false;
	}
	
	const WCHAR* pwszIdentity = pSubAccount->getIdentity();
	assert(pwszIdentity);
	
	FolderWait wait(this, pOutbox);
	
	MessagePtrList listMessagePtr;
	listMessagePtr.reserve(pOutbox->getCount());
	
	{
		Lock<Account> lock(*pOutbox->getAccount());
		
		const WCHAR* pwszMessageId = pItem->getMessageId();
		for (unsigned int n = 0; n < pOutbox->getCount(); ++n) {
			MessageHolder* pmh = pOutbox->getMessage(n);
			if (!pmh->isFlag(MessageHolder::FLAG_DRAFT) &&
				!pmh->isFlag(MessageHolder::FLAG_DELETED)) {
				bool bSend = true;
				
				if (pwszMessageId) {
					wstring_ptr wstrMessageId = pmh->getMessageId();
					bSend = wstrMessageId.get() && wcscmp(pwszMessageId, wstrMessageId.get()) == 0;
				}
				
				if (bSend && *pwszIdentity) {
					Message msg;
					if (!pmh->getMessage(Account::GMF_HEADER,
						L"X-QMAIL-SubAccount", SECURITYMODE_NONE, &msg)) {
						addError(pSyncManagerCallback, nId, pAccount, 0, 0, IDS_ERROR_GETMESSAGE, 0);
						return false;
					}
					
					UnstructuredParser subaccount;
					if (msg.getField(L"X-QMAIL-SubAccount", &subaccount) == Part::FIELD_EXIST) {
						SubAccount* p = pAccount->getSubAccount(subaccount.getValue());
						bSend = p && wcscmp(p->getIdentity(), pwszIdentity) == 0;
					}
					else {
						bSend = false;
					}
				}
				
				if (bSend)
					listMessagePtr.push_back(MessagePtr(pmh));
				
				if (pSyncManagerCallback->isCanceled(nId, false))
					return true;
			}
		}
	}
	if (listMessagePtr.empty())
		return true;
	
	std::auto_ptr<Logger> pLogger;
	if (pSubAccount->isLog(Account::HOST_SEND))
		pLogger = pAccount->openLogger(Account::HOST_SEND);
	
	std::auto_ptr<SendSession> pSession(SendSessionFactory::getSession(
		pAccount->getType(Account::HOST_SEND)));
	
	std::auto_ptr<SendSessionCallbackImpl> pCallback(
		new SendSessionCallbackImpl(pSyncManagerCallback, nId));
	if (!pSession->init(pDocument, pAccount, pSubAccount,
		pProfile_, pLogger.get(), pCallback.get()))
		return false;
	
	struct SendSessionTerm
	{
		SendSessionTerm(SendSession* pSession) :
			pSession_(pSession),
			bConnected_(false)
		{
		}
		
		~SendSessionTerm()
		{
			if (bConnected_)
				pSession_->disconnect();
			pSession_->term();
		}
		
		void setConnected()
		{
			bConnected_ = true;
		}
		
		SendSession* pSession_;
		bool bConnected_;
	} term(pSession.get());
	
	if (pSyncManagerCallback->isCanceled(nId, false))
		return true;
	
	if (!pSession->connect())
		return false;
	term.setConnected();
	if (pSyncManagerCallback->isCanceled(nId, false))
		return true;
	
	pCallback->setRange(0, static_cast<unsigned int>(listMessagePtr.size()));
	
	MessageHolderList l;
	l.resize(1);
	
	for (MessagePtrList::size_type m = 0; m < listMessagePtr.size(); ++m) {
		if (pSyncManagerCallback->isCanceled(nId, false))
			return true;
		
		Message msg;
		bool bGet = false;
		{
			MessagePtrLock mpl(listMessagePtr[m]);
			if (mpl) {
				if (!mpl->getMessage(Account::GMF_ALL, 0, SECURITYMODE_NONE, &msg)) {
					addError(pSyncManagerCallback, nId, pAccount, 0, 0, IDS_ERROR_GETMESSAGE, 0);
					return false;
				}
				bGet = true;
			}
		}
		if (bGet) {
			NormalFolder* pMessageSentbox = pSentbox;
			UnstructuredParser sentbox;
			if (msg.getField(L"X-QMAIL-Sentbox", &sentbox) == Part::FIELD_EXIST) {
				Folder* pFolder = pAccount->getFolder(sentbox.getValue());
				if (pFolder && pFolder->getType() == Folder::TYPE_NORMAL)
					pMessageSentbox = static_cast<NormalFolder*>(pFolder);
			}
			
			if (!pSession->sendMessage(&msg))
				return false;
			
			MessagePtrLock mpl(listMessagePtr[m]);
			if (mpl) {
				l[0] = mpl;
				unsigned int nCopyFlags = Account::COPYFLAG_MOVE |
					Account::OPFLAG_ACTIVE | Account::OPFLAG_BACKGROUND;
				if (!pAccount->setMessagesFlags(l,
						MessageHolder::FLAG_SENT, MessageHolder::FLAG_SENT, 0) ||
					!pAccount->copyMessages(l, pOutbox, pMessageSentbox, nCopyFlags, 0, 0, 0)) {
					addError(pSyncManagerCallback, nId, pAccount, 0, 0, IDS_ERROR_MOVETOSENTBOX, 0);
					return false;
				}
			}
		}
		
		pCallback->setPos(static_cast<unsigned int>(m + 1));
	}
	
	if (!pAccount->saveMessages(false)) {
		addError(pSyncManagerCallback, nId, pAccount, 0, 0, IDS_ERROR_SAVE, 0);
		return false;
	}
	
	return true;
}

bool qm::SyncManager::applyRules(unsigned int nId,
								 Document* pDocument,
								 SyncManagerCallback* pSyncManagerCallback,
								 const ApplyRulesSyncDataItem* pItem)
{
	assert(pDocument);
	assert(pSyncManagerCallback);
	assert(pItem);
	
	Account* pAccount = pItem->getAccount();
	SubAccount* pSubAccount = pItem->getSubAccount();
	
	Folder* pFolder = pItem->getFolder();
	MessagePtrList listMessagePtr;
	{
		Lock<Account> lock(*pAccount);
		const MessageHolderList& l = pFolder->getMessages();
		listMessagePtr.resize(l.size());
		std::copy(l.begin(), l.end(), listMessagePtr.begin());
	}
	RuleManager* pRuleManager = pDocument->getRuleManager();
	RuleCallbackImpl callback(pSyncManagerCallback, nId);
	if (!pRuleManager->applyAuto(pFolder, &listMessagePtr, pSubAccount,
		pDocument, pProfile_, RuleManager::AUTOFLAG_EXISTING, &callback))
		return false;
	
	return true;
}

bool qm::SyncManager::openReceiveSession(unsigned int nId,
										 Document* pDocument,
										 SyncManagerCallback* pSyncManagerCallback,
										 const SyncItem* pItem,
										 SyncData::Type type,
										 std::auto_ptr<ReceiveSession>* ppSession,
										 std::auto_ptr<ReceiveSessionCallback>* ppCallback,
										 std::auto_ptr<Logger>* ppLogger)
{
	assert(ppSession);
	assert(ppCallback);
	assert(ppLogger);
	
	ppSession->reset(0);
	
	Account* pAccount = pItem->getAccount();
	SubAccount* pSubAccount = pItem->getSubAccount();
	
	pSyncManagerCallback->setAccount(nId, pAccount, pSubAccount);
	
	std::auto_ptr<Logger> pLogger;
	if (pSubAccount->isLog(Account::HOST_RECEIVE))
		pLogger = pAccount->openLogger(Account::HOST_RECEIVE);
	
	std::auto_ptr<ReceiveSession> pSession(ReceiveSessionFactory::getSession(
		pAccount->getType(Account::HOST_RECEIVE)));
	if (!pSession.get())
		return false;
	
	std::auto_ptr<ReceiveSessionCallbackImpl> pCallback(new ReceiveSessionCallbackImpl(
		pSyncManagerCallback, nId, pSubAccount, pDocument, pProfile_, isNotify(type)));
	if (!pSession->init(pDocument, pAccount, pSubAccount,
		pProfile_, pLogger.get(), pCallback.get()))
		return false;
	
	*ppSession = pSession;
	*ppCallback = pCallback;
	*ppLogger = pLogger;
	
	return true;
}

bool qm::SyncManager::isNotify(SyncData::Type type) const
{
	switch (pProfile_->getInt(L"Sync", L"Notify")) {
	case NOTIFY_NEVER:
		return false;
	case NOTIFY_AUTO:
		return type == SyncData::TYPE_AUTO;
	case NOTIFY_ALWAYS:
	default:
		return true;
	}
}


/****************************************************************************
 *
 * SyncManager::SyncThread
 *
 */

qm::SyncManager::SyncThread::SyncThread(SyncManager* pSyncManager,
										std::auto_ptr<SyncData> pData) :
	pSyncManager_(pSyncManager),
	pSyncData_(pData),
	bWaitMode_(false)
{
}

qm::SyncManager::SyncThread::~SyncThread()
{
}

void qm::SyncManager::SyncThread::setWaitMode()
{
	bWaitMode_ = true;
}

void qm::SyncManager::SyncThread::run()
{
	InitThread init(0);
	
	struct StatusChange
	{
		StatusChange(SyncManager* pSyncManager) :
			pSyncManager_(pSyncManager)
		{
			pSyncManager_->fireStatusChanged(true);
		}
		
		~StatusChange()
		{
			pSyncManager_->fireStatusChanged(false);
		}
		
		SyncManager* pSyncManager_;
	} statusChange(pSyncManager_);
	
	pSyncManager_->syncData(pSyncData_.get());
	
	Lock<CriticalSection> lock(pSyncManager_->cs_);
	
	if (!bWaitMode_) {
		SyncManager::ThreadList::iterator it = std::find(
			pSyncManager_->listThread_.begin(),
			pSyncManager_->listThread_.end(), this);
		assert(it != pSyncManager_->listThread_.end());
		
		pSyncManager_->listThread_.erase(it);
		delete this;
	}
}


/****************************************************************************
 *
 * SyncManager::ParallelSyncThread
 *
 */

qm::SyncManager::ParallelSyncThread::ParallelSyncThread(SyncManager* pSyncManager,
														const SyncData* pData,
														const SyncData::ItemList& listItem) :
	pSyncManager_(pSyncManager),
	pSyncData_(pData),
	listItem_(listItem)
{
}

qm::SyncManager::ParallelSyncThread::~ParallelSyncThread()
{
}

void qm::SyncManager::ParallelSyncThread::run()
{
	InitThread init(0);
	
	pSyncManager_->syncSlotData(pSyncData_, listItem_);
}


/****************************************************************************
 *
 * SyncManager::ReceiveSessionCallbackImpl
 *
 */

qm::SyncManager::ReceiveSessionCallbackImpl::ReceiveSessionCallbackImpl(SyncManagerCallback* pCallback,
																		unsigned int nId,
																		SubAccount* pSubAccount,
																		Document* pDocument,
																		Profile* pProfile,
																		bool bNotify) :
	pCallback_(pCallback),
	nId_(nId),
	pSubAccount_(pSubAccount),
	pDocument_(pDocument),
	pProfile_(pProfile),
	bNotify_(bNotify)
{
	assert(pCallback);
	assert(pDocument);
	assert(pProfile);
}

qm::SyncManager::ReceiveSessionCallbackImpl::~ReceiveSessionCallbackImpl()
{
}

PasswordState qm::SyncManager::ReceiveSessionCallbackImpl::getPassword(SubAccount* pSubAccount,
																	   Account::Host host,
																	   wstring_ptr* pwstrPassword)
{
	return pCallback_->getPassword(pSubAccount, host, pwstrPassword);
}

void qm::SyncManager::ReceiveSessionCallbackImpl::setPassword(SubAccount* pSubAccount,
															  Account::Host host,
															  const WCHAR* pwszPassword,
															  bool bPermanent)
{
	pCallback_->setPassword(pSubAccount, host, pwszPassword, bPermanent);
}

void qm::SyncManager::ReceiveSessionCallbackImpl::addError(const SessionErrorInfo& info)
{
	pCallback_->addError(nId_, info);
}

bool qm::SyncManager::ReceiveSessionCallbackImpl::isCanceled(bool bForce)
{
	return pCallback_->isCanceled(nId_, bForce);
}

void qm::SyncManager::ReceiveSessionCallbackImpl::setPos(size_t n)
{
	pCallback_->setPos(nId_, false, n);
}

void qm::SyncManager::ReceiveSessionCallbackImpl::setRange(size_t nMin,
														   size_t nMax)
{
	pCallback_->setRange(nId_, false, nMin, nMax);
}

void qm::SyncManager::ReceiveSessionCallbackImpl::setSubPos(size_t n)
{
	pCallback_->setPos(nId_, true, n);
}

void qm::SyncManager::ReceiveSessionCallbackImpl::setSubRange(size_t nMin,
															  size_t nMax)
{
	pCallback_->setRange(nId_, true, nMin, nMax);
}

void qm::SyncManager::ReceiveSessionCallbackImpl::setMessage(const WCHAR* pwszMessage)
{
	pCallback_->setMessage(nId_, pwszMessage);
}

void qm::SyncManager::ReceiveSessionCallbackImpl::notifyNewMessage(MessagePtr ptr)
{
	if (!bNotify_)
		return;
	
	Recents* pRecents = pDocument_->getRecents();
	
	std::auto_ptr<MessageHolderURI> pURI;
	{
		MessagePtrLock mpl(ptr);
		if (mpl && !mpl->getFolder()->isFlag(Folder::FLAG_IGNOREUNSEEN)) {
			const Macro* pMacro = pRecents->getFilter();
			if (pMacro) {
				Message msg;
				MacroVariableHolder globalVariable;
				MacroContext context(mpl, &msg, mpl->getAccount(), pSubAccount_,
					MessageHolderList(), mpl->getFolder(), pDocument_, 0, 0, pProfile_,
					0, MacroContext::FLAG_NONE, SECURITYMODE_NONE, 0, &globalVariable);
				MacroValuePtr pValue(pMacro->value(&context));
				if (pValue.get() && pValue->boolean())
					pURI.reset(new MessageHolderURI(mpl));
			}
			else {
				pURI.reset(new MessageHolderURI(mpl));
			}
		}
	}
	if (pURI.get()) {
		pRecents->add(pURI);
		pCallback_->notifyNewMessage(nId_);
	}
}


/****************************************************************************
 *
 * SyncManager::SendSessionCallbackImpl
 *
 */

qm::SyncManager::SendSessionCallbackImpl::SendSessionCallbackImpl(SyncManagerCallback* pCallback,
																  unsigned int nId) :
	pCallback_(pCallback),
	nId_(nId)
{
}

qm::SyncManager::SendSessionCallbackImpl::~SendSessionCallbackImpl()
{
}

PasswordState qm::SyncManager::SendSessionCallbackImpl::getPassword(SubAccount* pSubAccount,
																	Account::Host host,
																	wstring_ptr* pwstrPassword)
{
	return pCallback_->getPassword(pSubAccount, host, pwstrPassword);
}

void qm::SyncManager::SendSessionCallbackImpl::setPassword(SubAccount* pSubAccount,
														   Account::Host host,
														   const WCHAR* pwszPassword,
														   bool bPermanent)
{
	pCallback_->setPassword(pSubAccount, host, pwszPassword, bPermanent);
}

void qm::SyncManager::SendSessionCallbackImpl::addError(const SessionErrorInfo& info)
{
	pCallback_->addError(nId_, info);
}

bool qm::SyncManager::SendSessionCallbackImpl::isCanceled(bool bForce)
{
	return pCallback_->isCanceled(nId_, bForce);
}

void qm::SyncManager::SendSessionCallbackImpl::setPos(size_t n)
{
	pCallback_->setPos(nId_, false, n);
}

void qm::SyncManager::SendSessionCallbackImpl::setRange(size_t nMin,
														size_t nMax)
{
	pCallback_->setRange(nId_, false, nMin, nMax);
}

void qm::SyncManager::SendSessionCallbackImpl::setSubPos(size_t n)
{
	pCallback_->setPos(nId_, true, n);
}

void qm::SyncManager::SendSessionCallbackImpl::setSubRange(size_t nMin,
														   size_t nMax)
{
	pCallback_->setRange(nId_, true, nMin, nMax);
}

void qm::SyncManager::SendSessionCallbackImpl::setMessage(const WCHAR* pwszMessage)
{
	pCallback_->setMessage(nId_, pwszMessage);
}


/****************************************************************************
 *
 * SyncManager::RasConnectionCallbackImpl
 *
 */

qm::SyncManager::RasConnectionCallbackImpl::RasConnectionCallbackImpl(const SyncDialup* pDialup,
																	  SyncManagerCallback* pCallback) :
	pDialup_(pDialup),
	pCallback_(pCallback)
{
}

qm::SyncManager::RasConnectionCallbackImpl::~RasConnectionCallbackImpl()
{
}

bool qm::SyncManager::RasConnectionCallbackImpl::isCanceled()
{
	return pCallback_->isCanceled(-1, false);
}

bool qm::SyncManager::RasConnectionCallbackImpl::preConnect(RASDIALPARAMS* prdp)
{
	if (pDialup_->getFlags() & SyncDialup::FLAG_SHOWDIALOG)
		return pCallback_->showDialupDialog(prdp);
	else
		return true;
}

void qm::SyncManager::RasConnectionCallbackImpl::setMessage(const WCHAR* pwszMessage)
{
	pCallback_->setMessage(-1, pwszMessage);
}

void qm::SyncManager::RasConnectionCallbackImpl::error(const WCHAR* pwszMessage)
{
	SyncManager::addError(pCallback_, -1, 0, 0, 0, IDS_ERROR_DIALUP, pwszMessage);
}


/****************************************************************************
 *
 * SyncManager::RuleCallbackImpl
 *
 */

qm::SyncManager::RuleCallbackImpl::RuleCallbackImpl(SyncManagerCallback* pCallback,
													unsigned int nId) :
	pCallback_(pCallback),
	nId_(nId)
{
}

qm::SyncManager::RuleCallbackImpl::~RuleCallbackImpl()
{
}

bool qm::SyncManager::RuleCallbackImpl::isCanceled()
{
	return pCallback_->isCanceled(nId_, false);
}

void qm::SyncManager::RuleCallbackImpl::checkingMessages(Folder* pFolder)
{
	wstring_ptr wstrMessage(getMessage(IDS_MESSAGE_CHECKMESSAGES, pFolder));
	pCallback_->setMessage(nId_, wstrMessage.get());
}

void qm::SyncManager::RuleCallbackImpl::applyingRule(Folder* pFolder)
{
	wstring_ptr wstrMessage(getMessage(IDS_MESSAGE_APPLYRULE, pFolder));
	pCallback_->setMessage(nId_, wstrMessage.get());
}

void qm::SyncManager::RuleCallbackImpl::setRange(size_t nMin,
												 size_t nMax)
{
	pCallback_->setRange(nId_, false, nMin, nMax);
}

void qm::SyncManager::RuleCallbackImpl::setPos(size_t nPos)
{
	pCallback_->setPos(nId_, false, nPos);
}

wstring_ptr qm::SyncManager::RuleCallbackImpl::getMessage(UINT nId,
														  Folder* pFolder)
{
	wstring_ptr wstrMessage(loadString(getResourceHandle(), nId));
	wstring_ptr wstrName(pFolder->getFullName());
	return concat(wstrMessage.get(), L" : ", wstrName.get());
}


/****************************************************************************
 *
 * SyncManager::FolderWait
 *
 */

qm::SyncManager::FolderWait::FolderWait(SyncManager* pSyncManager,
										NormalFolder* pFolder) :
	pSyncManager_(pSyncManager)
{
	assert(pFolder);
	
	std::auto_ptr<Event> pEvent;
	{
		typedef SyncManager::SyncingFolderList List;
		List& l = pSyncManager_->listSyncingFolder_;
		
		Lock<CriticalSection> lock(pSyncManager_->cs_);
		List::reverse_iterator it = std::find_if(l.rbegin(), l.rend(),
			boost::bind(&List::value_type::first, _1) == pFolder);
		if (it != l.rend()) {
			pEvent.reset((*it).second);
			l.erase(it.base() - 1);
		}
		std::auto_ptr<Event> pEvent(new Event(false, false));
		l.push_back(std::make_pair(pFolder, pEvent.get()));
		pEvent_ = pEvent.release();
	}
	if (pEvent.get())
		pEvent->wait();
}

qm::SyncManager::FolderWait::~FolderWait()
{
	typedef SyncManager::SyncingFolderList List;
	List& l = pSyncManager_->listSyncingFolder_;
	
	Lock<CriticalSection> lock(pSyncManager_->cs_);
	List::iterator it = std::find_if(l.begin(), l.end(),
		boost::bind(&List::value_type::second, _1) == pEvent_);
	if (it != l.end()) {
		delete (*it).second;
		l.erase(it);
	}
	else {
		pEvent_->set();
	}
}


/****************************************************************************
 *
 * SyncManagerCallback
 *
 */

qm::SyncManagerCallback::~SyncManagerCallback()
{
}


/****************************************************************************
 *
 * SyncManagerHandler
 *
 */

qm::SyncManagerHandler::~SyncManagerHandler()
{
}


/****************************************************************************
 *
 * SyncManagerEvent
 *
 */

qm::SyncManagerEvent::SyncManagerEvent(SyncManager* pSyncManager,
									   Status status) :
	pSyncManager_(pSyncManager),
	status_(status)
{
}

qm::SyncManagerEvent::~SyncManagerEvent()
{
}

SyncManager* qm::SyncManagerEvent::getSyncManager() const
{
	return pSyncManager_;
}

SyncManagerEvent::Status qm::SyncManagerEvent::getStatus() const
{
	return status_;
}
