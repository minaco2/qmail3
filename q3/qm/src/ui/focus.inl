/*
 * $Id$
 *
 * Copyright(C) 1998-2008 Satoshi Nakamura
 *
 */

#ifndef __FOCUS_INL__
#define __FOCUS_INL__


/****************************************************************************
 *
 * FocusController
 *
 */

template<class Item>
qm::FocusController<Item>::~FocusController()
{
}

#endif // __FOCUS_INL__
