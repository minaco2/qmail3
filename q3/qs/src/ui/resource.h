//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by qs.rc
//
#define IDC_LINK                        100
#define IDB_BROWSEFOLDER                200
#define IDS_RAS_OPENPORT                1001
#define IDS_RAS_PORTOPENED              1002
#define IDS_RAS_CONNECTDEVICE           1003
#define IDS_RAS_DEVICECONNECTED         1004
#define IDS_RAS_AUTHENTICATE            1005
#define IDS_RAS_AUTHENTICATED           1006
#define IDS_RAS_CONNECTED               1007
#define IDS_RAS_WAITINGBEFOREDISCONNECTING 1008
#define IDS_RAS_DISCONNECTING           1009
#define IDS_REGULAR                     1010
#define IDS_BOLD                        1011
#define IDS_ITALIC                      1012
#define IDS_BOLDITALIC                  1013

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
