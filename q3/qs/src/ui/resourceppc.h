//{{NO_DEPENDENCIES}}
// Microsoft eMbedded Visual C++ generated include file.
// Used by qsppc.rc
//
#define IDR_EMPTY                       101
#define IDR_OK                          102
#define IDD_BROWSEFOLDER                101
#define IDD_FOLDERNAME                  102
#define IDD_FONT                        103
#define IDD_BROWSEFOLDER_L              601
#define IDD_FOLDERNAME_L                602
#define IDD_FONT_L                      603
#define IDC_FOLDER                      1000
#define IDC_NEWFOLDER                   1001
#define IDC_FOLDERNAME                  1002
#define IDC_FONTFACE                    1003
#define IDC_FONTSIZE                    1004
#define IDC_FONTSTYLE                   1005
#define IDC_FOLDERNAMELABEL             1006
#define IDC_FONTFACELABEL               1007
#define IDC_FONTSTYLELABEL              1008
#define IDC_FONTSIZELABEL               1009
#define IDS_ERROR_CREATEFOLDER          10001
#define IDM_EMPTY                       40004
#define IDS_EMPTY                       40005

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        105
#define _APS_NEXT_COMMAND_VALUE         40006
#define _APS_NEXT_CONTROL_VALUE         1010
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
