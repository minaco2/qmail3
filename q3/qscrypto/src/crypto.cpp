/*
 * $Id$
 *
 * Copyright(C) 1998-2008 Satoshi Nakamura
 *
 */

#include <qsconv.h>
#include <qsinit.h>
#include <qslog.h>
#include <qsstl.h>

#include <stdio.h>

#if !defined _WIN32_WCE || _WIN32_WCE > 0x300 || defined PLATFORM_PPC2002
#	include <tchar.h>
#	include <wincrypt.h>
#endif

#include <openssl/pem.h>

#include "crypto.h"
#include "smime.h"
#include "util.h"

using namespace qscrypto;
using namespace qs;


/****************************************************************************
 *
 * Global functions
 *
 */

extern "C" int passwordCallback(char* pBuf,
								int nSize,
								int nRWFlag,
								void* pParam);


/****************************************************************************
 *
 * NameImpl
 *
 */

qscrypto::NameImpl::NameImpl(X509_NAME* pName) :
	pName_(pName)
{
}

qscrypto::NameImpl::~NameImpl()
{
}

wstring_ptr qscrypto::NameImpl::getText() const
{
	char buf[1024];
	X509_NAME_oneline(pName_, buf, sizeof(buf) - 1);
	buf[sizeof(buf) - 1] = '\0';
	return mbs2wcs(buf);
}

wstring_ptr qscrypto::NameImpl::getCommonName() const
{
	return getText(NID_commonName);
}

wstring_ptr qscrypto::NameImpl::getEmailAddress() const
{
	return getText(NID_pkcs9_emailAddress);
}

wstring_ptr qscrypto::NameImpl::getText(int nid) const
{
	int nLen = X509_NAME_get_text_by_NID(pName_, nid, 0, 0);
	if (nLen == -1)
		return 0;
	
	string_ptr strText(allocString(nLen + 1));
	if (X509_NAME_get_text_by_NID(pName_, nid, strText.get(), nLen + 1) == -1)
		return 0;
	
	return mbs2wcs(strText.get());
}


/****************************************************************************
 *
 * GeneralNameImpl
 *
 */

qscrypto::GeneralNameImpl::GeneralNameImpl(GENERAL_NAME* pGeneralName) :
	pGeneralName_(pGeneralName)
{
}

qscrypto::GeneralNameImpl::~GeneralNameImpl()
{
}

GeneralName::Type qscrypto::GeneralNameImpl::getType() const
{
	switch (pGeneralName_->type) {
	case GEN_DNS:
		return TYPE_DNS;
	case GEN_EMAIL:
		return TYPE_EMAIL;
	default:
		return TYPE_OTHER;
	}
}

wstring_ptr qscrypto::GeneralNameImpl::getValue() const
{
	switch (pGeneralName_->type) {
	case GEN_DNS:
		return mbs2wcs(reinterpret_cast<char*>(pGeneralName_->d.dNSName->data),
			pGeneralName_->d.dNSName->length);
	case GEN_EMAIL:
		return mbs2wcs(reinterpret_cast<char*>(pGeneralName_->d.rfc822Name->data),
			pGeneralName_->d.rfc822Name->length);
	default:
		return 0;
	}
}


/****************************************************************************
 *
 * GeneralNamesImpl
 *
 */

qscrypto::GeneralNamesImpl::GeneralNamesImpl(GENERAL_NAMES* pGeneralNames) :
	pGeneralNames_(pGeneralNames)
{
}

qscrypto::GeneralNamesImpl::~GeneralNamesImpl()
{
	sk_GENERAL_NAME_pop_free(pGeneralNames_, GENERAL_NAME_free);
}

int qscrypto::GeneralNamesImpl::getCount() const
{
	return sk_GENERAL_NAME_num(pGeneralNames_);
}

std::auto_ptr<GeneralName> qscrypto::GeneralNamesImpl::getGeneralName(int nIndex) const
{
	GENERAL_NAME* pGeneralName = sk_GENERAL_NAME_value(pGeneralNames_, nIndex);
	return std::auto_ptr<GeneralName>(new GeneralNameImpl(pGeneralName));
}


/****************************************************************************
 *
 * CertificateImpl
 *
 */

qscrypto::CertificateImpl::CertificateImpl() :
	pX509_(0),
	bFree_(true)
{
}

qscrypto::CertificateImpl::CertificateImpl(X509* pX509,
										   bool bDuplicate) :
	pX509_(pX509),
	bFree_(false)
{
	if (bDuplicate) {
		pX509_ = X509_dup(pX509);
		bFree_ = true;
	}
}

qscrypto::CertificateImpl::~CertificateImpl()
{
	if (bFree_ && pX509_)
		X509_free(pX509_);
}

X509* qscrypto::CertificateImpl::getX509() const
{
	return pX509_;
}

X509* qscrypto::CertificateImpl::releaseX509()
{
	X509* p = pX509_;
	pX509_ = 0;
	return p;
}

bool qscrypto::CertificateImpl::load(const WCHAR* pwszPath,
									 FileType type,
									 CryptoPasswordCallback* pCallback)
{
	assert(pwszPath);
	assert(!pX509_);
	
	Log log(InitThread::getInitThread().getLogger(), L"qscrypto::CertificateImpl");
	
	log.debugf(L"Loading a certificate from %s", pwszPath);
	
	FileInputStream stream(pwszPath);
	if (!stream) {
		log.errorf(L"Failed to open file: %s", pwszPath);
		return false;
	}
	return load(&stream, type, pCallback);
}

bool qscrypto::CertificateImpl::load(InputStream* pStream,
									 FileType type,
									 CryptoPasswordCallback* pCallback)
{
	assert(pStream);
	assert(!pX509_);
	
	Log log(InitThread::getInitThread().getLogger(), L"qscrypto::CertificateImpl");
	
	malloc_size_ptr<unsigned char> buf(Util::loadFromStream(pStream));
	if (!buf.get()) {
		log.error(L"Failed to read from stream.");
		return false;
	}
	
	BIOPtr pIn(BIO_new_mem_buf(buf.get(), static_cast<int>(buf.size())));
	
	switch (type) {
	case FILETYPE_PEM:
		pX509_ = PEM_read_bio_X509(pIn.get(), 0,
			pCallback ? &passwordCallback : 0, pCallback);
		if (!pX509_) {
			Util::logError(log, L"Failed to load a certificate from PEM.");
			return false;
		}
		break;
	default:
		assert(false);
		break;
	}
	
	return true;
}

bool qscrypto::CertificateImpl::save(const WCHAR* pwszPath,
									 FileType type) const
{
	assert(pwszPath);
	assert(pX509_);
	
	Log log(InitThread::getInitThread().getLogger(), L"qscrypto::CertificateImpl");
	
	log.debugf(L"Saving a certificate to %s", pwszPath);
	
	FileOutputStream stream(pwszPath);
	if (!stream) {
		log.errorf(L"Failed to open file: %s", pwszPath);
		return false;
	}
	return save(&stream, type);
}

bool qscrypto::CertificateImpl::save(OutputStream* pStream,
									 FileType type) const
{
	assert(pStream);
	assert(pX509_);
	
	Log log(InitThread::getInitThread().getLogger(), L"qscrypto::CertificateImpl");
	
	BIOPtr pOut(BIO_new(BIO_s_mem()));
	
	switch (type) {
	case FILETYPE_PEM:
		if (PEM_write_bio_X509(pOut.get(), pX509_) == 0) {
			Util::logError(log, L"Failed to save a certificate to PEM.");
			return false;
		}
		break;
	default:
		assert(false);
		break;
	}
	
	char* pBuf = 0;
	int nLen = BIO_get_mem_data(pOut.get(), &pBuf);
	if (pStream->write(reinterpret_cast<const unsigned char*>(pBuf), nLen) != nLen) {
		log.error(L"Failed to write to stream.");
		return false;
	}
	
	return true;
}

wstring_ptr qscrypto::CertificateImpl::getText() const
{
	assert(pX509_);
	
	BIOPtr pOut(BIO_new(BIO_s_mem()));
	if (X509_print(pOut.get(), pX509_) == 0)
		return 0;
	
	char* pBuf = 0;
	int nLen = BIO_get_mem_data(pOut.get(), &pBuf);
	
	return mbs2wcs(pBuf, nLen);
}

std::auto_ptr<Name> qscrypto::CertificateImpl::getSubject() const
{
	assert(pX509_);
	
	X509_NAME* pX509Name = X509_get_subject_name(pX509_);
	if (!pX509Name)
		return std::auto_ptr<Name>(0);
	return std::auto_ptr<Name>(new NameImpl(pX509Name));
}

std::auto_ptr<Name> qscrypto::CertificateImpl::getIssuer() const
{
	assert(pX509_);
	
	X509_NAME* pX509Name = X509_get_issuer_name(pX509_);
	if (!pX509Name)
		return std::auto_ptr<Name>(0);
	return std::auto_ptr<Name>(new NameImpl(pX509Name));
}

std::auto_ptr<GeneralNames> qscrypto::CertificateImpl::getSubjectAltNames() const
{
	assert(pX509_);
	
	GENERAL_NAMES* pGeneralNames = static_cast<GENERAL_NAMES*>(
		X509_get_ext_d2i(pX509_, NID_subject_alt_name, 0, 0));
	if (!pGeneralNames)
		return std::auto_ptr<GeneralNames>();
	return std::auto_ptr<GeneralNames>(new GeneralNamesImpl(pGeneralNames));
}

bool qscrypto::CertificateImpl::checkValidity() const
{
	assert(pX509_);
	
	return X509_cmp_current_time(X509_get_notBefore(pX509_)) < 0 &&
		X509_cmp_current_time(X509_get_notAfter(pX509_)) > 0;
}

std::auto_ptr<Certificate> qscrypto::CertificateImpl::clone() const
{
	assert(pX509_);
	
	return std::auto_ptr<Certificate>(new CertificateImpl(pX509_, true));
}


/****************************************************************************
 *
 * PrivateKeyImpl
 *
 */

qscrypto::PrivateKeyImpl::PrivateKeyImpl() :
	pKey_(0)
{
}

qscrypto::PrivateKeyImpl::~PrivateKeyImpl()
{
	if (pKey_)
		EVP_PKEY_free(pKey_);
}

EVP_PKEY* qscrypto::PrivateKeyImpl::getKey() const
{
	return pKey_;
}

bool qscrypto::PrivateKeyImpl::load(const WCHAR* pwszPath,
									FileType type,
									CryptoPasswordCallback* pCallback)
{
	assert(pwszPath);
	assert(!pKey_);
	
	Log log(InitThread::getInitThread().getLogger(), L"qscrypto::PrivateKeyImpl");
	
	log.debugf(L"Loading a private key from %s", pwszPath);
	
	FileInputStream stream(pwszPath);
	if (!stream) {
		log.errorf(L"Failed to open file: %s", pwszPath);
		return false;
	}
	BufferedInputStream bufferedStream(&stream, false);
	
	return load(&bufferedStream, type, pCallback);
}

bool qscrypto::PrivateKeyImpl::load(InputStream* pStream,
									FileType type,
									CryptoPasswordCallback* pCallback)
{
	assert(pStream);
	assert(!pKey_);
	
	Log log(InitThread::getInitThread().getLogger(), L"qscrypto::PrivateKeyImpl");
	
	malloc_size_ptr<unsigned char> buf(Util::loadFromStream(pStream));
	if (!buf.get()) {
		log.error(L"Failed to read from stream.");
		return false;
	}
	
	BIOPtr pIn(BIO_new_mem_buf(buf.get(), static_cast<int>(buf.size())));
	
	switch (type) {
	case FILETYPE_PEM:
		pKey_ = PEM_read_bio_PrivateKey(pIn.get(), 0,
			pCallback ? &passwordCallback : 0, pCallback);
		if (!pKey_) {
			Util::logError(log, L"Failed to load a private key from PEM.");
			return false;
		}
		break;
	default:
		assert(false);
		break;
	}
	
	return true;
}


/****************************************************************************
 *
 * PublicKeyImpl
 *
 */

qscrypto::PublicKeyImpl::PublicKeyImpl() :
	pKey_(0)
{
}

qscrypto::PublicKeyImpl::~PublicKeyImpl()
{
	if (pKey_)
		EVP_PKEY_free(pKey_);
}

EVP_PKEY* qscrypto::PublicKeyImpl::getKey() const
{
	return pKey_;
}

bool qscrypto::PublicKeyImpl::load(const WCHAR* pwszPath,
								   FileType type,
								   CryptoPasswordCallback* pCallback)
{
	assert(pwszPath);
	assert(!pKey_);
	
	Log log(InitThread::getInitThread().getLogger(), L"qscrypto::PublicKeyImpl");
	
	log.debugf(L"Loading a public key from %s", pwszPath);
	
	FileInputStream stream(pwszPath);
	if (!stream) {
		log.errorf(L"Failed to open file: %s", pwszPath);
		return false;
	}
	BufferedInputStream bufferedStream(&stream, false);
	
	return load(&bufferedStream, type, pCallback);
}

bool qscrypto::PublicKeyImpl::load(InputStream* pStream,
								   FileType type,
								   CryptoPasswordCallback* pCallback)
{
	assert(pStream);
	assert(!pKey_);
	
	Log log(InitThread::getInitThread().getLogger(), L"qscrypto::PublicKeyImpl");
	
	malloc_size_ptr<unsigned char> buf(Util::loadFromStream(pStream));
	if (!buf.get()) {
		log.error(L"Failed to read from stream.");
		return false;
	}
	
	BIOPtr pIn(BIO_new_mem_buf(buf.get(), static_cast<int>(buf.size())));
	
	switch (type) {
	case FILETYPE_PEM:
		pKey_ = PEM_read_bio_PUBKEY(pIn.get(), 0,
			pCallback ? &passwordCallback : 0, pCallback);
		if (!pKey_) {
			Util::logError(log, L"Failed to load a public key from PEM.");
			return false;
		}
		break;
	default:
		assert(false);
		break;
	}
	
	return true;
}


/****************************************************************************
 *
 * StoreImpl
 *
 */

qscrypto::StoreImpl::StoreImpl() :
	pStore_(0)
{
	pStore_ = X509_STORE_new();
	if (!pStore_)
		return;
}

qscrypto::StoreImpl::~StoreImpl()
{
	if (pStore_)
		X509_STORE_free(pStore_);
}

X509_STORE* qscrypto::StoreImpl::getStore() const
{
	return pStore_;
}

bool qscrypto::StoreImpl::load(const WCHAR* pwszPath,
							   FileType type)
{
	assert(pwszPath);
	
	Log log(InitThread::getInitThread().getLogger(), L"qscrypto::StoreImpl");
	
#if 1
	switch (type) {
	case FILETYPE_PEM:
		{
			FileInputStream stream(pwszPath);
			if (!stream) {
				log.errorf(L"Failed to open file: %s", pwszPath);
				return false;
			}
			BufferedInputStream bufferedStream(&stream, false);
			
			malloc_size_ptr<unsigned char> buf(Util::loadFromStream(&bufferedStream));
			if (!buf.get()) {
				log.error(L"Failed to read from stream.");
				return false;
			}
			
			BIOPtr pIn(BIO_new_mem_buf(buf.get(), static_cast<int>(buf.size())));
			
			STACK_OF(X509_INFO)* pStackInfo =
				PEM_X509_INFO_read_bio(pIn.get(), 0, 0, 0);
			if (!pStackInfo) {
				Util::logError(log, L"Failed to load certificates from PEM.");
				return false;
			}
			for (int n = 0; n < sk_X509_INFO_num(pStackInfo); ++n) {
				X509_INFO* pInfo = sk_X509_INFO_value(pStackInfo, n);
				if (pInfo->x509 && CertificateImpl(pInfo->x509, false).checkValidity())
					X509_STORE_add_cert(pStore_, pInfo->x509);
				if (pInfo->crl)
					X509_STORE_add_crl(pStore_, pInfo->crl);
			}
			sk_X509_INFO_pop_free(pStackInfo, X509_INFO_free);
		}
		break;
	default:
		assert(false);
		break;
	}
#else
	string_ptr strPath(wcs2mbs(pwszPath));
	
	X509_LOOKUP* pLookup = X509_STORE_add_lookup(pStore_, X509_LOOKUP_file());
	if (!pLookup) {
		Util::logError(log, L"X509_STORE_add_lookup failed");
		return false;
	}
	switch (type) {
	case FILETYPE_PEM:
		{
			int n = X509_LOOKUP_load_file(pLookup,
				strPath.get(), X509_FILETYPE_PEM);
			if (!n) {
				Util::logError(log, L"X509_LOOKUP_load_file failed");
				return false;
			}
		}
		break;
	default:
		assert(false);
		break;
	}
#endif
	
	return true;
}

bool qscrypto::StoreImpl::loadSystem()
{
#if !defined _WIN32_WCE || _WIN32_WCE > 0x300 || defined PLATFORM_PPC2002
	Log log(InitThread::getInitThread().getLogger(), L"qscrypto::StoreImpl");
	
	const TCHAR* ptszNames[] = {
		_T("ROOT"),
		_T("CA")
	};
	for (int n = 0; n < countof(ptszNames); ++n) {
		HCERTSTORE hStore = ::CertOpenSystemStore(0, ptszNames[n]);
		if (!hStore) {
			T2W(ptszNames[n], pwszName);
			log.warnf(L"Failed to open system store: ", pwszName);
			return false;
		}
		
		const CERT_CONTEXT* pContext = 0;
		while (true) {
			pContext = ::CertEnumCertificatesInStore(hStore, pContext);
			if (!pContext)
				break;
			
			const unsigned char* p = pContext->pbCertEncoded;
			X509Ptr x509(d2i_X509(0, &p, pContext->cbCertEncoded));
			if (!x509.get())
				continue;
			
			CertificateImpl cert(x509.get(), false);
			bool bAdd = cert.checkValidity();
			if (bAdd)
				X509_STORE_add_cert(pStore_, x509.get());
			
			if (log.isDebugEnabled()) {
				wstring_ptr wstrSubject;
				std::auto_ptr<Name> pSubject(cert.getSubject());
				if (pSubject.get())
					wstrSubject = pSubject->getText();
				wstring_ptr wstrIssuer;
				std::auto_ptr<Name> pIssuer(cert.getIssuer());
				if (pIssuer.get())
					wstrIssuer = pIssuer->getText();
				const WCHAR* pwszMessage = bAdd ?
					L"Added a certificate in the system store.\n" :
					L"Ignored an invalid certificate in the system store.\n";
				StringBuffer<WSTRING> buf(pwszMessage);
				buf.append(L"Subject: ");
				if (wstrSubject.get())
					buf.append(wstrSubject.get());
				buf.append(L"\nIssuer: ");
				if (wstrIssuer.get())
					buf.append(wstrIssuer.get());
				log.debug(buf.getCharArray());
			}
		}
		::CertFreeCertificateContext(pContext);
		::CertCloseStore(hStore, 0);
	}
#endif
	
	return true;
}


/****************************************************************************
 *
 * CipherImpl
 *
 */

qscrypto::CipherImpl::CipherImpl(const WCHAR* pwszName) :
	pCipher_(0)
{
	string_ptr strName(wcs2mbs(pwszName));
	pCipher_ = EVP_get_cipherbyname(strName.get());
}

qscrypto::CipherImpl::~CipherImpl()
{
}

const EVP_CIPHER* qscrypto::CipherImpl::getCipher() const
{
	return pCipher_;
}


/****************************************************************************
 *
 * CryptoFactoryImpl
 *
 */

CryptoFactoryImpl qscrypto::CryptoFactoryImpl::factory__;

qscrypto::CryptoFactoryImpl::CryptoFactoryImpl()
{
	registerFactory(this);
}

qscrypto::CryptoFactoryImpl::~CryptoFactoryImpl()
{
	unregisterFactory(this);
}

std::auto_ptr<Certificate> qscrypto::CryptoFactoryImpl::createCertificate()
{
	return std::auto_ptr<Certificate>(new CertificateImpl());
}

std::auto_ptr<PrivateKey> qscrypto::CryptoFactoryImpl::createPrivateKey()
{
	return std::auto_ptr<PrivateKey>(new PrivateKeyImpl());
}

std::auto_ptr<PublicKey> qscrypto::CryptoFactoryImpl::createPublicKey()
{
	return std::auto_ptr<PublicKey>(new PublicKeyImpl());
}

std::auto_ptr<Store> qscrypto::CryptoFactoryImpl::createStore()
{
	std::auto_ptr<StoreImpl> pStore(new StoreImpl());
	if (!pStore->getStore())
		return std::auto_ptr<Store>(0);
	return pStore;
}

std::auto_ptr<Cipher> qscrypto::CryptoFactoryImpl::createCipher(const WCHAR* pwszName)
{
	std::auto_ptr<CipherImpl> pCipher(new CipherImpl(pwszName));
	if (!pCipher->getCipher())
		return std::auto_ptr<Cipher>(0);
	return pCipher;
}

std::auto_ptr<SMIMEUtility> qscrypto::CryptoFactoryImpl::createSMIMEUtility()
{
	return std::auto_ptr<SMIMEUtility>(new SMIMEUtilityImpl());
}


/****************************************************************************
 *
 * Global functions
 *
 */

extern "C" int passwordCallback(char* pBuf,
								int nSize,
								int nRWFlag,
								void* pParam)
{
	CryptoPasswordCallback* pCallback = static_cast<CryptoPasswordCallback*>(pParam);
	
	QTRY {
		wstring_ptr wstrPassword(pCallback->getPassword());
		string_ptr strPassword(wcs2mbs(wstrPassword.get()));
		int nLen = static_cast<int>(strlen(strPassword.get()));
		if (nLen >= nSize)
			return 0;
		strcpy(pBuf, strPassword.get());
		return nLen;
	}
	QCATCH_ALL() {
		return 0;
	}
}
