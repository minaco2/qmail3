//{{NO_DEPENDENCIES}}
// Microsoft eMbedded Visual C++ generated include file.
// Used by qmnntpppc.rc
//
#define IDD_RECEIVE                     101
#define IDD_SEND                        102
#define IDD_SUBSCRIBE                   103
#define IDD_RECEIVE_L                   601
#define IDD_SEND_L                      602
#define IDD_SUBSCRIBE_L                 603
#define IDC_INITIALFETCHCOUNT           1001
#define IDC_XOVER                       1004
#define IDC_FETCHCOUNT                  1005
#define IDC_FILTER                      1006
#define IDC_INITIALFETCHCOUNTLABEL      1006
#define IDC_FETCHCOUNTLABEL             1007
#define IDC_GROUP                       1008
#define IDC_REFRESH                     1009
#define IDC_FILTERLABEL                 1010

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1011
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
