//{{NO_DEPENDENCIES}}
// Microsoft eMbedded Visual C++ generated include file.
// Used by qmnntp.rc
//
#define IDS_NNTP                        1000
#define IDS_FOLDER_OUTBOX               1100
#define IDS_FOLDER_POSTED               1101
#define IDS_FOLDER_TRASH                1102
#define IDS_FOLDER_JUNK                 1103
#define IDS_INITIALIZE                  2000
#define IDS_LOOKUP                      2001
#define IDS_CONNECTING                  2002
#define IDS_CONNECTED                   2003
#define IDS_AUTHENTICATING              2004
#define IDS_SELECTGROUP                 2005
#define IDS_DOWNLOADMESSAGES            2006
#define IDS_DOWNLOADRESERVEDMESSAGES    2007
#define IDS_MANAGEJUNK                  2008
#define IDS_FILTERJUNK                  2009
#define IDS_SUBSCRIBE                   2100
#define IDS_GROUP                       2101
#define IDS_REFRESH                     2102
#define IDS_ERROR_MESSAGE               10000
#define IDS_ERROR_GREETING              11000
#define IDS_ERROR_GROUP                 11001
#define IDS_ERROR_AUTHINFO              11002
#define IDS_ERROR_ARTICLE               11003
#define IDS_ERROR_HEAD                  11004
#define IDS_ERROR_BODY                  11005
#define IDS_ERROR_XOVER                 11006
#define IDS_ERROR_MODEREADER            11007
#define IDS_ERROR_POST                  11008
#define IDS_ERROR_LIST                  11009
#define IDS_ERROR_NEWGROUPS             11010
#define IDS_ERROR_INITIALIZE            12000
#define IDS_ERROR_CONNECT               12001
#define IDS_ERROR_RESPONSE              12002
#define IDS_ERROR_INVALIDSOCKET         12003
#define IDS_ERROR_OTHER                 12004
#define IDS_ERROR_SELECT                12005
#define IDS_ERROR_TIMEOUT               12006
#define IDS_ERROR_RECEIVE               12007
#define IDS_ERROR_DISCONNECT            12008
#define IDS_ERROR_SEND                  12009
#define IDS_ERROR_SSL                   12010
#define IDS_ERROR_SAVE                  14000
#define IDS_ERROR_APPLYRULES            14001
#define IDS_ERROR_SUBSCRIBE             14002
#define IDS_ERROR_REFRESH               14003
#define IDS_ERROR_MANAGEJUNK            14004
#define IDS_ERROR_FILTERJUNK            14005

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
