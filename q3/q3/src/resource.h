//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by qmail.rc
//
#define IDS_TITLE                       1
#define IDS_ERROR_LOADLIBRARY           100
#define IDS_ERROR_GETPROCADDRESS        101
#define IDI_QMAIL                       101

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
